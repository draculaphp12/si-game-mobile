package ru.bespalov.main_menu

import android.app.Activity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.common.R
import ru.bespalov.navigation.ScreenNavigation

@Composable
fun MainMenu(navController: NavController) {
    val activity = (LocalContext.current as? Activity)
    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        SiBackgroundImage(drawableResId = R.drawable.si_backgroud)

        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center
        ) {
            SiTextButton(buttonTextResourceId = R.string.create_host) {
                navController.navigate(ScreenNavigation.CreateHostScreen.routeName)
            }
            SiTextButton(buttonTextResourceId = R.string.connect_to_game) {
                navController.navigate(ScreenNavigation.ConnectToGameScreen.routeName)
            }
            SiTextButton(buttonTextResourceId = R.string.settings) {

            }
            SiTextButton(buttonTextResourceId = R.string.logout_button) {
                activity?.finishAndRemoveTask()
            }
        }
    }
}