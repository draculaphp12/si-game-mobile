package ru.bespalov.question

import ru.bespalov.common.mvi.UiState
import ru.bespalov.domain.model.Question

data class QuestionState(
    val question: Question?,
    val answerDialogState: Boolean,
    val playerAnswer: String,
    val isAnswerButtonEnable: Boolean,
    val isTimerPaused: Boolean,
    val timerProgress: Float
) : UiState {
    companion object {
        val initialState = QuestionState(
            question = null,
            answerDialogState = false,
            playerAnswer = "",
            isAnswerButtonEnable = true,
            isTimerPaused = false,
            timerProgress = 1f
        )
    }
}