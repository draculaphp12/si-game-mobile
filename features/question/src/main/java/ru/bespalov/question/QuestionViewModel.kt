package ru.bespalov.question

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import ru.bespalov.common.mvi.BaseViewModel
import ru.bespalov.common.mvi.Reducer
import ru.bespalov.data.currentUserId
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.usecase.GetGameFlowUseCase
import ru.bespalov.domain.usecase.GetQuestionUseCase
import ru.bespalov.domain.usecase.UpdateCurrentRespondentUseCase
import ru.bespalov.domain.usecase.UpdateLastQuestionIdUseCase
import ru.bespalov.domain.usecase.answer.AnswerTimeOutUseCase
import ru.bespalov.domain.usecase.answer.CheckPlayerFalseAnswerUseCase
import ru.bespalov.domain.usecase.answer.FalseAnswerUseCase
import ru.bespalov.domain.usecase.answer.RightAnswerUseCase
import ru.bespalov.question.QuestionUIEvent.*

class QuestionViewModel(
    private val questionId: String?,
    private val getQuestionUseCase: GetQuestionUseCase,
    private val updateCurrentRespondentUseCase: UpdateCurrentRespondentUseCase,
    private val getGameFlowUseCase: GetGameFlowUseCase,
    private val checkPlayerFalseAnswerUseCase: CheckPlayerFalseAnswerUseCase,
    private val updateLastQuestionIdUseCase: UpdateLastQuestionIdUseCase,
    private val rightAnswerUseCase: RightAnswerUseCase,
    private val falseAnswerUseCase: FalseAnswerUseCase,
    private val answerTimeOutUseCase: AnswerTimeOutUseCase
) : BaseViewModel<QuestionState, QuestionUIEvent>() {

    override val state: Flow<QuestionState>
        get() = reducer.state

    init {
        loadQuestion()
        checkPlayerFalseAnswer()
    }

    fun sendEvent(event: QuestionUIEvent) {
        reducer.sendEvent(event)
    }

    private var isPlayerFalseAnswerQuestion = false
    private var isNavigationMade = false
    private var respondent: String? = null

    private fun loadQuestion() {
        viewModelScope.launch {
            if (questionId != null) {
                val question = getQuestionUseCase.execute(questionId)
                sendEvent(OnQuestionLoaded(question))
            }
        }
    }

    private fun checkPlayerFalseAnswer() {
        viewModelScope.launch {
            if (questionId != null) {
                isPlayerFalseAnswerQuestion = checkPlayerFalseAnswerUseCase.execute(questionId, currentUserId)
                if (isPlayerFalseAnswerQuestion) {
                    sendEvent(OnChangeAnswerButtonState(false))
                }
            }
        }
    }

    private fun observeGameState(gameId: String, navController: NavController) {
        viewModelScope.launch {
            getGameFlowUseCase.execute(gameId).collect { game ->
                game?.let {
                    if (game.currentRespondentId != null && currentUserId != respondent) {
                        if (game.currentRespondentId != currentUserId) {
                            sendEvent(OnChangeAnswerButtonState(false))
                        } else {
                            sendEvent(OnChangeAnswerDialogState(true))
                        }
                        sendEvent(OnUpdateTimerPaused(true))
                    } else {
                        if (game.currentRespondentId == null) {
                            sendEvent(OnUpdateTimerPaused(false))
                        }
                        sendEvent(
                            OnChangeAnswerButtonState(
                                game.currentRespondentId == null && !isPlayerFalseAnswerQuestion
                            )
                        )
                    }
                    respondent = game.currentRespondentId
                    if (game.currentQuestionId == null && !isNavigationMade) {
                        if (questionId != null) {
                            updateLastQuestionIdUseCase.execute(gameId, questionId)
                        }
                        navController.popBackStack()
                        isNavigationMade = true
                    }
                }
            }
        }
    }

    private fun answerQuestion(context: Context, gameId: String, playerAnswer: String, question: Question?) {
        viewModelScope.launch {
            if (questionId !== null && question !== null) {
                val isAnswerRight = playerAnswer.contains(question.rightAnswer, true)
                if (isAnswerRight) {
                    rightAnswerUseCase.execute(
                        playerAnswer,
                        currentUserId,
                        gameId,
                        question
                    )
                    Toast.makeText(context, "Right Answer", Toast.LENGTH_LONG).show()
                } else {
                    falseAnswerUseCase.execute(
                        playerAnswer,
                        currentUserId,
                        gameId,
                        question
                    ) {
                        isPlayerFalseAnswerQuestion = true
                        sendEvent(OnChangeAnswerButtonState(false))
                    }
                    Toast.makeText(context, "False Answer", Toast.LENGTH_LONG).show()
                }
                sendEvent(OnChangeAnswerDialogState(false))
            }
        }
    }

    private fun updateRespondent(gameId: String, playerId: String?) {
        viewModelScope.launch {
            updateCurrentRespondentUseCase.execute(gameId, playerId)
        }
    }

    private fun observeQuestionTimer(gameId: String, timerProgress: Float) {
        viewModelScope.launch {
            if (timerProgress <= 0.0f) {
                answerTimeOutUseCase.execute(gameId, questionId)
            }
        }
    }

    private val reducer = QuestionReducer(QuestionState.initialState)

    private inner class QuestionReducer(
        initial: QuestionState
    ) : Reducer<QuestionState, QuestionUIEvent>(initial) {
        override fun reduce(oldState: QuestionState, event: QuestionUIEvent) {
            when (event) {
                is OnAnswerButtonClick -> {
                    updateRespondent(event.gameId, event.playerId)
                }
                is OnAnswerDialogButtonClick -> {
                    answerQuestion(event.context, event.gameId, event.answer, event.question)
                }
                is OnCloseDialogButtonClick -> {
                    answerQuestion(event.context, event.gameId, event.answer, event.question)
                }
                is OnScreenLoaded -> {
                    observeGameState(event.gameId, event.navController)
                    observeQuestionTimer(event.gameId, event.timerProgress)
                }
                is OnChangeAnswerButtonState -> {
                    setState(oldState.copy(isAnswerButtonEnable = event.isAnswerButtonEnable))
                }
                is OnChangeAnswerDialogState -> {
                    setState(oldState.copy(answerDialogState = event.answerDialogState))
                }
                is OnChangePlayerAnswer -> {
                    setState(oldState.copy(playerAnswer = event.answer))
                }
                is OnUpdateTimerProgress -> {
                    setState(oldState.copy(timerProgress = event.timerProgress))
                }
                is OnUpdateTimerPaused -> {
                    setState(oldState.copy(isTimerPaused = event.isTimerPaused))
                }
                is OnQuestionLoaded -> {
                    setState(oldState.copy(question = event.question))
                }
            }
        }
    }
}