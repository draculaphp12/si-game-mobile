package ru.bespalov.question

import android.widget.Toast
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.ui.StyledPlayerView
import ru.bespalov.common.R
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.data.currentUserId
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.model.QuestionContent
import ru.bespalov.question.view.SiAnswerDialog
import ru.bespalov.question.view.SiTimer

@Composable
fun QuestionScreen(
    questionId: String?,
    gameId: String?,
    navController: NavController,
    sendEvent: (QuestionUIEvent) -> Unit,
    state: QuestionState,
) {
    val context = LocalContext.current
    if (questionId != null && state.question !== null && gameId !== null) {
        SiAnswerDialog(
            dialogState = state.answerDialogState,
            state.playerAnswer,
            answerOnClickListener =  {
                sendEvent(
                    QuestionUIEvent.OnAnswerDialogButtonClick(
                        context,
                        gameId,
                        state.playerAnswer,
                        state.question
                    )
                )
            },
            onCloseClick = {
                sendEvent(
                    QuestionUIEvent.OnCloseDialogButtonClick(
                        context,
                        gameId,
                        null,
                        state.playerAnswer,
                        state.question
                    )
                )
            },
            sendEvent
        )

        sendEvent(QuestionUIEvent.OnScreenLoaded(gameId, navController, state.timerProgress))

        Box(
            Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            SiBackgroundImage(drawableResId = R.drawable.si_backgroud)

            Column(
                modifier = Modifier
                    .padding(dimensionResource(id = R.dimen.padding_16dp))
                    .fillMaxWidth(),
                verticalArrangement = Arrangement.Center
            ) {
                RenderQuestionContent(question = state.question)
                if (state.isAnswerButtonEnable) {
                    SiTextButton(buttonTextResourceId = R.string.ask_question_button) {
                        Toast.makeText(context, state.question.rightAnswer, Toast.LENGTH_LONG).show()
                        sendEvent(QuestionUIEvent.OnAnswerButtonClick(gameId, currentUserId))
                    }
                }
                SiTimer(state.timerProgress, state.isTimerPaused, sendEvent)
            }
        }
    }
}

@Composable
private fun RenderQuestionContent(
    question: Question
) {
    question.content.forEach {
        when (it) {
            is QuestionContent.AudioQuestionContent -> {
                RenderAudioContent(
                    fileName = it.content
                )
            }
            is QuestionContent.ImageQuestionContent -> {
                RenderImageContent(
                    imageContent = it
                )
            }
            is QuestionContent.VideoQuestionContent -> {
                RenderExoPlayer(
                    fileName = it.content,
                    modifier = Modifier
                )
            }
            is QuestionContent.TextQuestionContent -> {
                RenderTextContent(text = it.content)
            }
            is QuestionContent.SayQuestionContent -> {
                RenderTextContent(text = it.content)
            }
        }
    }
}

@Composable
private fun RenderAudioContent(
    fileName: String,
) {
    AudioNoteAnimation()
    RenderExoPlayer(fileName = fileName, modifier = Modifier.height(100.dp))
}


@Composable
private fun AudioNoteAnimation() {
    val animated by remember { mutableStateOf(true) }
    val rotation = remember { Animatable(initialValue = 360f) }

    LaunchedEffect(animated) {
        rotation.animateTo(
            targetValue = if (animated) 0f else 360f,
            animationSpec = infiniteRepeatable(
                animation = tween(durationMillis = 5000)
            ),
        )
    }
    AsyncImage(
        model = ImageRequest.Builder(LocalContext.current)
            .data(R.drawable.note)
            .crossfade(true)
            .build(),
        contentDescription = null,
        Modifier
            .padding(bottom = 35.dp)
            .fillMaxWidth()
            .graphicsLayer {
                rotationY = rotation.value
            },
        contentScale = ContentScale.FillWidth
    )
}

@Composable
private fun RenderTextContent(
    text: String
) {
    Text(
        text = text,
        modifier = Modifier.padding(8.dp),
        color = Color.White,
        fontSize = 25.sp
    )
}

@Composable
private fun RenderImageContent(
    imageContent: QuestionContent.ImageQuestionContent
) {
    val fileName = imageContent.content
    AsyncImage(
        model = ImageRequest.Builder(LocalContext.current)
            .data(fileName)
            .crossfade(true)
            .build(),
        contentDescription = null,
        Modifier.fillMaxWidth(),
        contentScale = ContentScale.FillWidth
    )
}

@Composable
private fun RenderExoPlayer(fileName: String, modifier: Modifier) {
    val context = LocalContext.current
    val exoPlayer = ExoPlayer.Builder(context).build()

    exoPlayer.also {
        val mediaItem = MediaItem.Builder()
            .setUri(fileName)
            .build()
        exoPlayer.clearMediaItems()
        exoPlayer.setMediaItem(mediaItem)
        exoPlayer.prepare()
    }

    DisposableEffect(
        AndroidView(
            factory = {
                StyledPlayerView(context).apply {
                    player = exoPlayer
                }
            },
            modifier = modifier
        )
    ) {
        onDispose { exoPlayer.release() }
    }
}