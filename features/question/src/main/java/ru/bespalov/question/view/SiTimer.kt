package ru.bespalov.question.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import ru.bespalov.question.QuestionUIEvent
import kotlin.time.Duration.Companion.seconds

@Composable
fun SiTimer(
    progress: Float = 1f,
    isPaused: Boolean,
    sendEvent: (QuestionUIEvent) -> Unit
) {
    var timerProgress = progress
    LaunchedEffect(isPaused) {
        while (timerProgress > 0.0f && !isPaused) {
            delay(1.seconds)
            timerProgress -= 0.05f
            sendEvent(QuestionUIEvent.OnUpdateTimerProgress(timerProgress))
        }
    }
    Column(modifier = Modifier.fillMaxWidth()) {
        LinearProgressIndicator(
            modifier = Modifier
                .fillMaxWidth()
                .height(15.dp),
            backgroundColor = Color.LightGray,
            color = Color.White,
            progress = timerProgress
        )
    }
}