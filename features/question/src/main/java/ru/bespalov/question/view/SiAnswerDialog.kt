package ru.bespalov.question.view

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import ru.bespalov.common.R
import ru.bespalov.common.view.SiDialog
import ru.bespalov.common.view.SiDialogButton
import ru.bespalov.question.QuestionUIEvent

@Composable
fun SiAnswerDialog(
    dialogState: Boolean,
    answer: String,
    answerOnClickListener: () -> Unit,
    onCloseClick: () -> Unit,
    sendEvent: (QuestionUIEvent) -> Unit
) {
    if (dialogState) {
        Dialog(
            onDismissRequest = {
                sendEvent(QuestionUIEvent.OnChangeAnswerDialogState(false))
            },
            content = {
                SiDialog(
                    content = { SiAnswerDialogContent(answer, answerOnClickListener, sendEvent) },
                    onCloseClick
                )
            },
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false
            )
        )
    }
}

@Composable
private fun SiAnswerDialogContent(
    answer: String,
    answerOnClickListener: () -> Unit,
    sendEvent: (QuestionUIEvent) -> Unit
) {
    Column {
        TextField(
            value = answer,
            onValueChange = {
                sendEvent(QuestionUIEvent.OnChangePlayerAnswer(it))
            },
            label = {
                Text(
                    "Ответ: ",
                    color = Color.Black
                )
            },
            modifier = Modifier
                .padding(vertical = dimensionResource(id = R.dimen.padding_8dp))
                .border(
                    BorderStroke(dimensionResource(id = R.dimen.border_1dp), Color.LightGray)
                )
                .fillMaxWidth(),
            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Black,
                backgroundColor = Color.LightGray
            )
        )
        SiDialogButton(onClick = answerOnClickListener)
    }
}