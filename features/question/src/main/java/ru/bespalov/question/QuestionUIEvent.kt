package ru.bespalov.question

import android.content.Context
import androidx.navigation.NavController
import ru.bespalov.common.mvi.UiEvent
import ru.bespalov.domain.model.Question

sealed class QuestionUIEvent : UiEvent {
    class OnAnswerDialogButtonClick(
        val context: Context,
        val gameId: String,
        val answer: String,
        val question: Question?
    ): QuestionUIEvent()
    class OnCloseDialogButtonClick(
        val context: Context,
        val gameId: String,
        val playerId: String?,
        val answer: String,
        val question: Question?
    ): QuestionUIEvent()
    class OnAnswerButtonClick(val gameId: String, val playerId: String?): QuestionUIEvent()
    class OnScreenLoaded(val gameId: String, val navController: NavController, val timerProgress: Float): QuestionUIEvent()
    class OnChangeAnswerButtonState(val isAnswerButtonEnable: Boolean): QuestionUIEvent()
    class OnChangeAnswerDialogState(val answerDialogState: Boolean): QuestionUIEvent()
    class OnChangePlayerAnswer(val answer: String): QuestionUIEvent()
    class OnUpdateTimerProgress(val timerProgress: Float): QuestionUIEvent()
    class OnUpdateTimerPaused(val isTimerPaused: Boolean): QuestionUIEvent()
    class OnQuestionLoaded(val question: Question?): QuestionUIEvent()
}