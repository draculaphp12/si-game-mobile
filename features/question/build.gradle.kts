plugins {
    id("common-android")
}

dependencies {
    implementation(Dependencies.exoPlayer)
    implementation(Dependencies.coilCompose)
}