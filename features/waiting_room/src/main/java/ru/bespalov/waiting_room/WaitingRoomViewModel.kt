package ru.bespalov.waiting_room

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.bespalov.domain.model.Player
import ru.bespalov.domain.usecase.InitGameRoomUseCase

class WaitingRoomViewModel(
    private val gameId: String?,
    private val initGameRoomUseCase: InitGameRoomUseCase
) : ViewModel() {
    var playersList = mutableStateOf<List<Player>>(emptyList())

    init {
        viewModelScope.launch(Dispatchers.IO) {
            if (!gameId.isNullOrEmpty()) {
                initGameRoomUseCase
                    .execute(gameId, viewModelScope)
                    .collect { playersFromDb ->
                        try {
                            playersList.value = playersFromDb
                        } catch (e: Exception) {
                            withContext(Dispatchers.Main) { playersList.value = emptyList() }
                        }
                    }
            }
        }
    }
}