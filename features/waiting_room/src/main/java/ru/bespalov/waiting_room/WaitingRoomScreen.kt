package ru.bespalov.waiting_room

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.domain.model.Player
import ru.bespalov.common.R
import ru.bespalov.navigation.ScreenNavigation


@Composable
fun WaitingRoomScreen(
    navController: NavController,
    gameId: String?,
    playersList: MutableState<List<Player>>
) {
    val context = LocalContext.current
    Box(
        Modifier.fillMaxSize()
    ) {
        SiBackgroundImage(drawableResId = R.drawable.si_backgroud)
        Column(
            modifier = Modifier
                .padding(dimensionResource(id = R.dimen.padding_16dp))
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (playersList.value.isEmpty()) {
                CircularProgressIndicator(color = Color.White)
            } else {
                WaitingRoomList(playersList.value)
            }

            SiTextButton(buttonTextResourceId = R.string.start_game) {
                navController.navigate(ScreenNavigation.GameScreen.routeName + "/${gameId}")
            }
            SiTextButton(buttonTextResourceId = R.string.copy_id) {
                val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clipData = ClipData.newPlainText("gameId", gameId)
                clipboardManager.setPrimaryClip(clipData)
            }
        }
    }
}

@Composable
fun WaitingRoomList(playersList: List<Player>) {
    LazyColumn(
        modifier = Modifier
            .padding(dimensionResource(id = R.dimen.padding_16dp))
            .fillMaxWidth(),
    ) {
        items(
            count = playersList.count(),
            itemContent = {
                PlayersListItem(player = playersList[it])
            }
        )
    }
}

@Composable
fun PlayersListItem(player: Player) {
    Card(
        modifier = Modifier
            .padding(vertical = 8.dp)
            .fillMaxWidth(),
        elevation = 2.dp,
        shape = RoundedCornerShape(corner = CornerSize(16.dp))
    ) {
        Row {
            Image(
                painter = painterResource(id = R.drawable.si_backgroud),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .padding(8.dp)
                    .size(84.dp)
                    .clip(RoundedCornerShape(corner = CornerSize(16.dp)))
            )
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    modifier = Modifier
                        .padding(vertical = 16.dp),
                    text = player.nickname,
                    style = typography.h6
                )
            }
        }
    }
}