package ru.bespalov.connect_game

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.common.view.SiTextField
import ru.bespalov.common.R
import ru.bespalov.navigation.ScreenNavigation

@Composable
fun ConnectToGameScreen(
    navController: NavController,
    gameId: MutableState<String>,
    connectPlayer: (String) -> Unit
) {
    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        SiBackgroundImage(drawableResId = R.drawable.si_backgroud)

        Column(
            modifier = Modifier
                .padding(dimensionResource(id = R.dimen.padding_16dp))
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center
        ) {
            SiTextField(value = gameId.value.uppercase(), label = stringResource(R.string.game_id_label)) {
                gameId.value = it
            }
            if (gameId.value.isNotEmpty()) {
                SiTextButton(buttonTextResourceId = R.string.connect_to_game) {
                    connectPlayer(gameId.value)
                    navController.navigate(
                        ScreenNavigation.WaitingRoomScreen.routeName
                                + "?gameId=${gameId.value}"
                    )
                }
            }
        }
    }
}