package ru.bespalov.connect_game

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.bespalov.domain.usecase.ConnectPlayerUseCase

class ConnectToGameViewModel(
    private val connectPlayerUseCase: ConnectPlayerUseCase
) : ViewModel() {
    val gameId = mutableStateOf("")

    fun connectPlayer(gameId: String) {
        viewModelScope.launch {
            connectPlayerUseCase.execute(gameId)
        }
    }
}