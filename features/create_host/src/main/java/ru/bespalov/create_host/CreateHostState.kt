package ru.bespalov.create_host

import ru.bespalov.common.mvi.UiState
import ru.bespalov.data.model.firebase.PackNode

data class CreateHostState(
    val gameId: String,
    val pack: PackNode?,
    val playersCount: String,
    val roundsCount: Int,
    val uploadingProgress: Float,
    val errorMessage: String?
): UiState {
    companion object {
        val initialState = CreateHostState(
            gameId = "",
            pack = null,
            playersCount = "",
            roundsCount = 1,
            uploadingProgress = 0.0f,
            errorMessage = null
        )
    }
}
