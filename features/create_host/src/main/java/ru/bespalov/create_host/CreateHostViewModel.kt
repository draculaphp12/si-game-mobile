package ru.bespalov.create_host

import android.content.Context
import android.net.Uri
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import ru.bespalov.common.mvi.BaseViewModel
import ru.bespalov.common.mvi.Reducer
import ru.bespalov.data.SiPackParser
import ru.bespalov.data.SiPackParser.progressFlow
import ru.bespalov.domain.model.Pack
import ru.bespalov.domain.usecase.CreateGameWithPlayerUseCase

class CreateHostViewModel(
    private val createGameWithPlayerUseCase: CreateGameWithPlayerUseCase,
) : BaseViewModel<CreateHostState, CreateHostUIEvent>() {

    override val state: Flow<CreateHostState>
        get() = reducer.state

    init {
        viewModelScope.launch {
            progressFlow.collect { progress ->
                sendEvent(CreateHostUIEvent.UpdateUploadingProgress(progress))
            }
        }
    }

    fun sendEvent(event: CreateHostUIEvent) {
        reducer.sendEvent(event)
    }

    private fun createGameWithPlayer(
        maxPlayersCount: Int,
        roundsCount: Int,
        pack: Pack
    ): String {
        return createGameWithPlayerUseCase.execute(maxPlayersCount, roundsCount, pack)
    }

    private fun parseZip(context: Context, fileUri: Uri) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val packData = SiPackParser.parseZip(context, fileUri, viewModelScope)
                packData?.let {
                    sendEvent(
                        CreateHostUIEvent.PackParsed(
                            packData,
                            packData.rounds.count()
                        )
                    )
                }
            } catch (e: IllegalArgumentException) {
                sendEvent(CreateHostUIEvent.ErrorOccurred(e))
            }
        }
    }

    private val reducer = CreateHostReducer(CreateHostState.initialState)

    private inner class CreateHostReducer(
        initial: CreateHostState
    ) : Reducer<CreateHostState, CreateHostUIEvent>(initial) {
        override fun reduce(oldState: CreateHostState, event: CreateHostUIEvent) {
            when (event) {
                is CreateHostUIEvent.ZipPackPicked -> {
                    parseZip(event.context, event.fileUri)
                }
                is CreateHostUIEvent.CreateGameButtonClicked -> {
                    val gameId = createGameWithPlayer(event.playersCount, event.roundsCount, event.pack)
                    setState(oldState.copy(gameId = gameId))
                    event.onGameCreated(gameId)
                }
                is CreateHostUIEvent.PlayersCountChanged -> {
                    setState(oldState.copy(playersCount = event.playersCount))
                }
                is CreateHostUIEvent.UpdateUploadingProgress -> {
                    setState(oldState.copy(uploadingProgress = event.progress))
                }
                is CreateHostUIEvent.PackParsed -> {
                    setState(
                        oldState.copy(
                            pack = event.pack,
                            roundsCount = event.roundsCount,
                            errorMessage = null
                        )
                    )
                }
                is CreateHostUIEvent.ErrorOccurred -> {
                    setState(oldState.copy(errorMessage = event.e.message))
                }
            }
        }
    }
}