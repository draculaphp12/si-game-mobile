package ru.bespalov.create_host

import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.common.view.SiTextField
import ru.bespalov.data.model.firebase.toPack
import ru.bespalov.navigation.ScreenNavigation
import ru.bespalov.common.R

@Composable
fun CreateHostScreen(
    navController: NavController,
    sendEvent: (CreateHostUIEvent) -> Unit,
    state: CreateHostState
) {
    val context = LocalContext.current
    val pickFileLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.GetContent()
    ) { fileUri ->
        if (fileUri != null) {
            sendEvent(CreateHostUIEvent.ZipPackPicked(context, fileUri))
        }
    }

    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        SiBackgroundImage(drawableResId = R.drawable.si_backgroud)

        Column(
            modifier = Modifier
                .padding(dimensionResource(id = R.dimen.padding_16dp))
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center
        ) {
            SiTextField(
                value = state.playersCount.uppercase(),
                label = stringResource(R.string.players_count_label),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
            ) {
                sendEvent(CreateHostUIEvent.PlayersCountChanged(it))
            }
            if (state.uploadingProgress == 0.0f) {
                SiTextButton(R.string.upload_pack_button) {
                    pickFileLauncher.launch("*/*")
                }
            }
            if (state.errorMessage != null) {
                Text(
                    text = state.errorMessage,
                    modifier = Modifier.padding(8.dp),
                    color = Color.Red,
                    fontSize = 25.sp
                )
            }
            if (state.uploadingProgress != 1.0f && state.uploadingProgress != 0.0f) {
                Column(modifier = Modifier.fillMaxWidth()) {
                    LinearProgressIndicator(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(15.dp),
                        backgroundColor = Color.LightGray,
                        color = Color.White,
                        progress = state.uploadingProgress
                    )
                }
            }
            if (
                state.pack !== null
                && state.playersCount.isNotEmpty()
                && state.uploadingProgress == 1.0f
                && state.errorMessage == null
            ) {
                SiTextButton(R.string.create_host) {
                    sendEvent(
                        CreateHostUIEvent.CreateGameButtonClicked(
                            state.playersCount.toInt(),
                            state.roundsCount,
                            state.pack.toPack()
                        ) {
                            navController.navigate(
                                ScreenNavigation.WaitingRoomScreen.routeName
                                        + "?gameId=${it}"
                            )
                        }
                    )
                }
            }
        }
    }
}