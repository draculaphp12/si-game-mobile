package ru.bespalov.create_host

import android.content.Context
import android.net.Uri
import ru.bespalov.common.mvi.UiEvent
import ru.bespalov.data.model.firebase.PackNode
import ru.bespalov.domain.model.Pack

sealed class CreateHostUIEvent : UiEvent {
    class ZipPackPicked(val context: Context, val fileUri: Uri): CreateHostUIEvent()
    class CreateGameButtonClicked(
        val playersCount: Int,
        val roundsCount: Int,
        val pack: Pack,
        val onGameCreated: (String) -> Unit
    ): CreateHostUIEvent()
    class PlayersCountChanged(val playersCount: String): CreateHostUIEvent()
    class UpdateUploadingProgress(val progress: Float): CreateHostUIEvent()
    class PackParsed(val pack: PackNode, val roundsCount: Int): CreateHostUIEvent()
    class ErrorOccurred(val e: Throwable): CreateHostUIEvent()
}
