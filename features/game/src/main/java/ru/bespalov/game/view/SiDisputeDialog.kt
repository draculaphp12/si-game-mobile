package ru.bespalov.game.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import ru.bespalov.common.R
import ru.bespalov.common.view.SiDialog
import ru.bespalov.domain.model.Game
import ru.bespalov.domain.model.Question
import ru.bespalov.game.GameUiEvent

@Composable
fun SiDisputeDialog(
    dialogState: Boolean,
    playerAnswer: String,
    rightAnswer: String,
    onCloseClick: () -> Unit,
    sendEvent: (GameUiEvent) -> Unit,
    lastQuestion: Question?,
    game: Game?
) {
    if (dialogState) {
        Dialog(
            onDismissRequest = {
                sendEvent(GameUiEvent.OnDisputeDialogStateChanged(false))
            },
            content = {
                SiDialog(
                    content = {
                        SiModeratorDisputeDialogContent(
                            playerAnswer = playerAnswer,
                            rightAnswer = rightAnswer,
                            sendEvent = sendEvent,
                            lastQuestion = lastQuestion,
                            game = game
                        )
                    },
                    onCloseClick
                )
            },
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false
            )
        )
    }
}

@Composable
private fun SiModeratorDisputeDialogContent(
    playerAnswer: String,
    rightAnswer: String,
    sendEvent: (GameUiEvent) -> Unit,
    lastQuestion: Question?,
    game: Game?
) {
    Column {
        Text("Ответ пользователя: $playerAnswer", color = Color.Black)
        Text("Правильный ответ: $rightAnswer", color = Color.Black)
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            Button(
                onClick = {
                    sendEvent(GameUiEvent.OnDisputeDialogAccept(game, lastQuestion))
                }
            ) {
                Text(
                    text = stringResource(id = R.string.accept_button).uppercase(),
                    modifier = Modifier.padding(4.dp),
                    color = Color.White
                )
            }
            Spacer(Modifier.width(10.dp))
            Button(
                onClick = {
                    sendEvent(GameUiEvent.OnDisputeDialogRejectOrClose(lastQuestion))
                }
            ) {
                Text(
                    text = stringResource(id = R.string.reject_button).uppercase(),
                    modifier = Modifier.padding(4.dp),
                    color = Color.White
                )
            }
        }
    }
}