package ru.bespalov.game

import ru.bespalov.common.mvi.UiEvent
import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.model.Game
import ru.bespalov.domain.model.Player
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.model.Round

sealed class GameUiEvent : UiEvent {
    class OnDisputeDialogAccept(val game: Game?, val lastQuestion: Question?) : GameUiEvent()
    class OnDisputeDialogRejectOrClose(val lastQuestion: Question?) : GameUiEvent()
    class OnDisputeDialogStateChanged(val dialogState: Boolean): GameUiEvent()
    class OnOpenDisputeDialog(val lastQuestion: Question?) : GameUiEvent()
    class OnRoundEnded(val game: Game) : GameUiEvent()
    class OnOpenQuestion(val questionId: String): GameUiEvent()
    class CheckRound(val currentRound: Int, val round: Round?): GameUiEvent()
    class OnRoundLoaded(val round: Round): GameUiEvent()
    class OnPlayersLoaded(val players: List<Player>): GameUiEvent()
    class OnGameLoaded(val game: Game): GameUiEvent()
    class OnLastQuestionLoaded(val lastQuestion: Question?): GameUiEvent()
    class OnLastDisputantFalseAnswerLoaded(val disputantFalseAnswer: Answer?): GameUiEvent()
}