package ru.bespalov.game

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.domain.model.Player
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.model.Round
import ru.bespalov.common.R
import ru.bespalov.game.view.SiDisputeDialog

const val MAX_QUESTION_IN_ROW = 5

@Composable
fun GameScreen(
    gameId: String?,
    state: GameState,
    sendEvent: (GameUiEvent) -> Unit
) {
    if (gameId != null) {
        Box(
            Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            SiBackgroundImage(drawableResId = R.drawable.si_backgroud)

            Column(
                modifier = Modifier
                    .padding(dimensionResource(id = R.dimen.padding_16dp))
                    .fillMaxWidth(),
                verticalArrangement = Arrangement.Center
            ) {
                state.round?.let { QuestionsTable(it, sendEvent) }
                state.game?.let { sendEvent(
                    GameUiEvent.CheckRound(
                        state.game.currentRound,
                        state.round
                    )
                ) }
                if (state.isRoundEnded) {
                    SiTextButton(buttonTextResourceId = R.string.next_round) {
                        if (state.game != null) {
                            sendEvent(GameUiEvent.OnRoundEnded(state.game))
                        }
                    }
                }
                PlayersList(playersList = state.playersList)
                GameMenuButtons(sendEvent, state.lastQuestion)
            }

            if (state.lastQuestion?.rightAnswer != null && state.disputantFalseAnswer != null) {
                SiDisputeDialog(
                    dialogState = state.isDisputeDialogOpen,
                    playerAnswer = state.disputantFalseAnswer.playerAnswer,
                    rightAnswer = state.lastQuestion.rightAnswer,
                    onCloseClick = {
                        sendEvent(GameUiEvent.OnDisputeDialogRejectOrClose(state.lastQuestion))
                    },
                    sendEvent = sendEvent,
                    game = state.game,
                    lastQuestion = state.lastQuestion
                )
            }
        }
    }
}

@Composable
private fun RowScope.SiQuestionTableCell(
    question: Question,
    weight: Float,
    sendEvent: (GameUiEvent) -> Unit
) {
    var text = question.price.toString()
    if (question.isChecked) {
        text = ""
    }
    Text(
        text = text,
        Modifier
            .border(1.dp, Color.White)
            .weight(weight)
            .height(60.dp)
            .padding(8.dp)
            .clickable {
                if (!question.isChecked) {
                    sendEvent(GameUiEvent.OnOpenQuestion(question.id))
                }
            },
        color = Color.White
    )
}

@Composable
private fun RowScope.SiThemeTableCell(
    name: String,
    weight: Float
) {
    Text(
        text = name,
        Modifier
            .border(1.dp, Color.White)
            .weight(weight)
            .height(60.dp)
            .padding(8.dp)
        ,
        color = Color.White
    )
}

@Composable
private fun QuestionsTable(
    round: Round,
    sendEvent: (GameUiEvent) -> Unit
) {
    val themeNameColumnWeight = .3f // 30%
    val questionPriceColumnWeight = .14f // 14%
    val themes = round.themes

    LazyColumn(
        Modifier
            .fillMaxWidth()
            .padding(
                start = 16.dp,
                end = 16.dp,
                top = 8.dp,
                bottom = 8.dp
            )
            .background(Color.Blue)
    ) {
        items(themes) {
            val questions = it.questions
            Row(Modifier.fillMaxWidth()) {
                SiThemeTableCell(name = it.name, weight = themeNameColumnWeight)

                questions.take(MAX_QUESTION_IN_ROW).forEach { question ->
                    SiQuestionTableCell(
                        question = question,
                        weight = questionPriceColumnWeight,
                        sendEvent = sendEvent
                    )
                }
            }
        }
    }
}

@Composable
private fun PlayersList(playersList: List<Player>) {
    LazyRow(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                start = 16.dp,
                end = 16.dp,
                top = 8.dp,
                bottom = 0.dp
            )
    ) {
        items(playersList) { player ->
            Card(
                modifier = Modifier
                    .padding(8.dp)
                    .width(120.dp),
                elevation = 6.dp
            ) {
                Column(
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Spacer(modifier = Modifier.height(3.dp))

                    Image(
                        painter = painterResource(id = R.drawable.ic_launcher_background),
                        contentDescription = "img",
                        modifier = Modifier
                            .height(60.dp)
                            .width(60.dp)
                            .padding(3.dp),
                        alignment = Alignment.Center
                    )
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = player.nickname,
                        modifier = Modifier.padding(2.dp),
                        color = Color.Black, textAlign = TextAlign.Center
                    )
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = player.score.toString(),
                        modifier = Modifier.padding(2.dp),
                        color = Color.Black, textAlign = TextAlign.Center
                    )
                }
            }
        }
    }
}

@Composable
private fun GameMenuButtons(
    sendEvent: (GameUiEvent) -> Unit,
    lastQuestion: Question?
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                start = dimensionResource(id = R.dimen.padding_16dp),
                end = dimensionResource(id = R.dimen.padding_16dp),
                top = 16.dp,
                bottom = 5.dp
            )
    ) {
        TextButton(
            modifier = Modifier
                .weight(2f, true)
                .border(
                    BorderStroke(1.dp, Color.White)
                ),
            onClick = {

            }
        ) {
            Text(
                text = stringResource(id = R.string.pause_button).uppercase(),
                modifier = Modifier.padding(4.dp),
                color = Color.White
            )
        }
        Spacer(Modifier.weight(1f))
        TextButton(
            modifier = Modifier
                .weight(2f, true)
                .border(
                    BorderStroke(1.dp, Color.White)
                ),
            onClick = {
                sendEvent(GameUiEvent.OnOpenDisputeDialog(lastQuestion))
            }
        ) {
            Text(
                text = stringResource(id = R.string.dispute_button).uppercase(),
                modifier = Modifier.padding(4.dp),
                color = Color.White
            )
        }
    }
}