package ru.bespalov.game

import ru.bespalov.common.mvi.UiState
import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.model.Game
import ru.bespalov.domain.model.Player
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.model.Round

data class GameState(
    val round: Round?,
    val game: Game?,
    val playersList: List<Player>,
    val isDisputeDialogOpen: Boolean,
    val disputantFalseAnswer: Answer?,
    val lastQuestion: Question?,
    val isRoundEnded: Boolean
): UiState {
    companion object {
        val initialState = GameState(
            round = null,
            game = null,
            playersList = emptyList(),
            isDisputeDialogOpen = false,
            disputantFalseAnswer = null,
            lastQuestion = null,
            isRoundEnded = false
        )
    }
}
