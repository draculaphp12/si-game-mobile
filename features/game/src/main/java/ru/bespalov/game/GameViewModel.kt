package ru.bespalov.game

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import ru.bespalov.common.mvi.BaseViewModel
import ru.bespalov.common.mvi.Reducer
import ru.bespalov.data.currentUserId
import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.model.Game
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.model.Round
import ru.bespalov.domain.usecase.GetGameFlowUseCase
import ru.bespalov.domain.usecase.GetPlayersUseCase
import ru.bespalov.domain.usecase.GetQuestionUseCase
import ru.bespalov.domain.usecase.GetRoundUseCase
import ru.bespalov.domain.usecase.IncreaseCurrentRoundUseCase
import ru.bespalov.domain.usecase.UpdateCurrentQuestionUseCase
import ru.bespalov.domain.usecase.answer.CheckAnswersUseCase
import ru.bespalov.domain.usecase.answer.GetPlayerAnswerUseCase
import ru.bespalov.domain.usecase.dispute.AcceptDisputeUseCase
import ru.bespalov.domain.usecase.dispute.CloseDisputeUseCase
import ru.bespalov.domain.usecase.dispute.OpenDisputeUseCase

class GameViewModel(
    private val gameId: String?,
    private val getGameFlowUseCase: GetGameFlowUseCase,
    private val getRoundUseCase: GetRoundUseCase,
    private val getPlayersUseCase: GetPlayersUseCase,
    private val updateCurrentQuestionUseCase: UpdateCurrentQuestionUseCase,
    private val navController: NavController,
    private val checkAnswersUseCase: CheckAnswersUseCase,
    private val getQuestionUseCase: GetQuestionUseCase,
    private val getPlayerAnswerUseCase: GetPlayerAnswerUseCase,
    private val increaseCurrentRoundUseCase: IncreaseCurrentRoundUseCase,
    private val acceptDisputeUseCase: AcceptDisputeUseCase,
    private val openDisputeUseCase: OpenDisputeUseCase,
    private val closeDisputeUseCase: CloseDisputeUseCase
) : BaseViewModel<GameState, GameUiEvent>() {

    override val state: Flow<GameState>
        get() = reducer.state

    init {
        loadGame()
        loadPlayers()
    }

    fun sendEvent(event: GameUiEvent) {
        reducer.sendEvent(event)
    }

    private var currentQuestionId: String? = null
    private var anotherPlayerRightAnswer = mutableStateOf<Answer?>(null)

    private fun loadGame() {
        viewModelScope.launch {
            if (gameId != null) {
                checkAnswersUseCase.execute(gameId)
                getGameFlowUseCase.execute(gameId).collect { gameItem ->
                    gameItem?.let {
                        sendEvent(GameUiEvent.OnGameLoaded(gameItem))
                        if (gameItem.lastQuestionId != null) {
                            sendEvent(
                                GameUiEvent.OnLastQuestionLoaded(
                                    getQuestionUseCase.execute(gameItem.lastQuestionId!!)
                                )
                            )
                            if (gameItem.currentDisputantId != null) {
                                sendEvent(
                                    GameUiEvent.OnLastDisputantFalseAnswerLoaded(
                                        getPlayerAnswerUseCase.execute(
                                            gameItem.lastQuestionId!!,
                                            gameItem.currentDisputantId,
                                            0
                                        )
                                    )
                                )
                            }
                            anotherPlayerRightAnswer.value = getPlayerAnswerUseCase.execute(
                                gameItem.lastQuestionId!!
                            )
                        }
                    }
                    if (
                        gameItem !== null
                        && gameItem.currentQuestionId != null
                        && gameItem.currentQuestionId != currentQuestionId
                    ) {
                        navController.navigate(
                            ru.bespalov.navigation.ScreenNavigation.QuestionScreen.routeName
                                    + "?questionId=${gameItem.currentQuestionId}"
                                    + "&gameId=${gameId}"
                        )
                        currentQuestionId = gameItem.currentQuestionId
                    }

                    if (gameItem !== null && gameItem.isDisputeOpen != null) {
                        sendEvent(GameUiEvent.OnDisputeDialogStateChanged(gameItem.isDisputeOpen!!))
                    }
                }
            }
        }
    }

    private fun checkIsRoundEnded(round: Round?): Boolean {
        if (round == null) {
            return false
        }
        val questions = mutableListOf<Question>()
        val themes = round.themes
        themes.forEach {
            questions.addAll(it.questions)
        }
        return questions.none { !it.isChecked }
    }

    private fun increaseRound(game: Game) {
        viewModelScope.launch {
            with(game) {
                increaseCurrentRoundUseCase.execute(id, currentRound, roundsCount)
            }
        }
    }

    private fun loadPlayers() {
        viewModelScope.launch {
            if (gameId !== null) {
                getPlayersUseCase
                    .execute(gameId)
                    .collect { playersFromDb ->
                        try {
                            sendEvent(GameUiEvent.OnPlayersLoaded(playersFromDb))
                        } catch (e: Exception) {
                            sendEvent(GameUiEvent.OnPlayersLoaded(emptyList()))
                        }
                    }
            }
        }
    }

    private fun loadRound(roundNumber: Int) {
        if (gameId !== null) {
            viewModelScope.launch {
                getRoundUseCase.execute(roundNumber, gameId).collect { roundList ->
                    if (roundList.isNotEmpty()) {
                        sendEvent(GameUiEvent.OnRoundLoaded(roundList.first()))
                    }
                }
            }
        }
    }

    private fun updateCurrentQuestion(questionId: String?) {
        viewModelScope.launch {
            if (gameId !== null) {
                updateCurrentQuestionUseCase.execute(gameId, questionId)
            }
        }
    }

    private fun onDisputeClickListener(lastQuestion: Question?) {
        viewModelScope.launch {
            if (gameId != null && lastQuestion != null) {
                openDisputeUseCase.execute(lastQuestion, currentUserId, gameId)
            }
        }
    }

    private fun onDisputeDialogAccept(game: Game?, lastQuestion: Question?) {
        viewModelScope.launch {
            if (game != null) {
                acceptDisputeUseCase.execute(
                    game,
                    lastQuestion,
                    currentUserId,
                    anotherPlayerRightAnswer.value
                )
            }
        }
    }

    private fun onDisputeDialogClose(lastQuestion: Question?) {
        viewModelScope.launch {
            if (gameId != null && lastQuestion != null) {
                closeDisputeUseCase.execute(gameId, lastQuestion.id, currentUserId)
            }
        }
    }

    private val reducer = GameReducer(GameState.initialState)

    private inner class GameReducer(
        initial: GameState
    ) : Reducer<GameState, GameUiEvent>(initial) {
        override fun reduce(oldState: GameState, event: GameUiEvent) {
            when (event) {
                is GameUiEvent.OnDisputeDialogStateChanged -> {
                    setState(oldState.copy(isDisputeDialogOpen = event.dialogState))
                }
                is GameUiEvent.OnDisputeDialogAccept -> {
                    onDisputeDialogAccept(event.game, event.lastQuestion)
                }
                is GameUiEvent.OnDisputeDialogRejectOrClose -> {
                    onDisputeDialogClose(event.lastQuestion)
                }
                is GameUiEvent.OnOpenDisputeDialog -> {
                    onDisputeClickListener(event.lastQuestion)
                }
                is GameUiEvent.OnRoundEnded -> {
                    increaseRound(event.game)
                }
                is GameUiEvent.OnOpenQuestion -> {
                    updateCurrentQuestion(event.questionId)
                }
                is GameUiEvent.CheckRound -> {
                    val isRoundEnded = checkIsRoundEnded(event.round)
                    setState(oldState.copy(isRoundEnded = isRoundEnded))
                    loadRound(event.currentRound)
                }
                is GameUiEvent.OnRoundLoaded -> {
                    setState(oldState.copy(round = event.round))
                }
                is GameUiEvent.OnPlayersLoaded -> {
                    setState(oldState.copy(playersList = event.players))
                }
                is GameUiEvent.OnGameLoaded -> {
                    setState(oldState.copy(game = event.game))
                }
                is GameUiEvent.OnLastQuestionLoaded -> {
                    setState(oldState.copy(lastQuestion = event.lastQuestion))
                }
                is GameUiEvent.OnLastDisputantFalseAnswerLoaded -> {
                    setState(oldState.copy(disputantFalseAnswer = event.disputantFalseAnswer))
                }
            }
        }
    }
}