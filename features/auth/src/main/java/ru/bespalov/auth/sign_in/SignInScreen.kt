package ru.bespalov.auth.sign_in

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import ru.bespalov.common.R
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.common.view.SiTextField
import ru.bespalov.domain.model.User
import ru.bespalov.navigation.ScreenNavigation

@Composable
fun SignInScreen(
    navController: NavController,
    state: SignInState,
    sendEvent: (SignInEvent) -> Unit
) {
    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        SiBackgroundImage(drawableResId = R.drawable.si_backgroud)

        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center
        ) {
            SiTextField(
                value = state.email,
                label = "Email: ",
                onValueChange = {
                    sendEvent(SignInEvent.OnEmailChanged(it))
                }
            )
            SiTextField(
                value = state.password,
                label = "Password: ",
                onValueChange = {
                    sendEvent(SignInEvent.OnPasswordChanged(it))
                }
            )
            if (state.email.isNotEmpty() && state.password.isNotEmpty()) {
                SiTextButton(buttonTextResourceId = R.string.sign_in_button) {
                    sendEvent(
                        SignInEvent.SignInEventButtonClicked(
                            user = User(state.email, state.password),
                            onSuccess = {
                                navController.navigate(ScreenNavigation.MainMenuScreen.routeName)
                            }
                        )
                    )
                }
            }
        }
    }
}