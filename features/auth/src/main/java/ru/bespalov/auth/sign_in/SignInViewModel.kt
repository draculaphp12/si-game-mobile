package ru.bespalov.auth.sign_in

import kotlinx.coroutines.flow.Flow
import ru.bespalov.common.mvi.BaseViewModel
import ru.bespalov.common.mvi.Reducer
import ru.bespalov.domain.repository.AuthRepository

class SignInViewModel(
    private val authRepository: AuthRepository
) : BaseViewModel<SignInState, SignInEvent>() {
    override val state: Flow<SignInState>
        get() = reducer.state

    fun sendEvent(event: SignInEvent) {
        reducer.sendEvent(event)
    }

    private val reducer = SignInReducer(SignInState.initialState)

    private inner class SignInReducer(
        initialState: SignInState
    ): Reducer<SignInState, SignInEvent>(initialState) {
        override fun reduce(oldState: SignInState, event: SignInEvent) {
            when (event) {
                is SignInEvent.OnEmailChanged -> {
                    setState(oldState.copy(email = event.email))
                }
                is SignInEvent.OnPasswordChanged -> {
                    setState(oldState.copy(password = event.password))
                }
                is SignInEvent.SignInEventButtonClicked -> {
                    authRepository.signIn(event.user, event.onSuccess) { error ->
                        setState(oldState.copy(errorMessage = error))
                    }
                }
            }
        }

    }
}