package ru.bespalov.auth.registration

import kotlinx.coroutines.flow.Flow
import ru.bespalov.common.mvi.BaseViewModel
import ru.bespalov.common.mvi.Reducer
import ru.bespalov.domain.repository.AuthRepository

class RegistrationViewModel(
    private val authRepository: AuthRepository
) : BaseViewModel<RegistrationState, RegistrationEvent>() {
    override val state: Flow<RegistrationState>
        get() = reducer.state

    fun sendEvent(event: RegistrationEvent) {
        reducer.sendEvent(event)
    }

    private val reducer = RegistrationReducer(RegistrationState.initialState)

    private inner class RegistrationReducer(
        initialState: RegistrationState
    ) : Reducer<RegistrationState, RegistrationEvent>(initialState) {
        override fun reduce(oldState: RegistrationState, event: RegistrationEvent) {
            when (event) {
                is RegistrationEvent.RegistrationButtonClicked -> {
                    authRepository.registration(event.user, event.onSuccess) { error ->
                        setState(oldState.copy(errorMessage = error))
                    }
                }
                is RegistrationEvent.OnEmailChanged -> {
                    setState(oldState.copy(email = event.email))
                }
                is RegistrationEvent.OnNicknameChanged -> {
                    setState(oldState.copy(nickname = event.nickname))
                }
                is RegistrationEvent.OnPasswordChanged -> {
                    setState(oldState.copy(password = event.password))
                }
            }
        }
    }
}