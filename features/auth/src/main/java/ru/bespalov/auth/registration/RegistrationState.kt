package ru.bespalov.auth.registration

import ru.bespalov.common.mvi.UiState

data class RegistrationState(
    val email: String,
    val password: String,
    val nickname: String,
    val errorMessage: String? = null
): UiState {
    companion object {
        val initialState = RegistrationState(
            "",
            "",
        ""
        )
    }
}