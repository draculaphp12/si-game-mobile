package ru.bespalov.auth.sign_in

import ru.bespalov.common.mvi.UiState

data class SignInState(
    val email: String,
    val password: String,
    val errorMessage: String? = null
) : UiState {
    companion object {
        val initialState = SignInState(
            email = "",
            password = "",
            errorMessage = null
        )
    }
}