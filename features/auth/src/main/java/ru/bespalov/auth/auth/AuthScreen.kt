package ru.bespalov.auth.auth

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import ru.bespalov.common.R
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.navigation.ScreenNavigation

@Composable
fun AuthScreen(
    navController: NavController,
    currentUserId: String?
) {
    if (currentUserId != null) {
        LaunchedEffect(Unit) {
            navController.navigate(ScreenNavigation.MainMenuScreen.routeName)
        }
    } else {
        Box(
            Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            SiBackgroundImage(drawableResId = R.drawable.si_backgroud)

            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                verticalArrangement = Arrangement.Center
            ) {
                SiTextButton(buttonTextResourceId = R.string.sign_in_button) {
                    navController.navigate(ScreenNavigation.SignInScreen.routeName)
                }
                SiTextButton(buttonTextResourceId = R.string.registration_button) {
                    navController.navigate(ScreenNavigation.RegistrationScreen.routeName)
                }
            }
        }
    }
}