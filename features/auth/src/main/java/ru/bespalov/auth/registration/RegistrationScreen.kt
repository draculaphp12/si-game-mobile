package ru.bespalov.auth.registration

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import ru.bespalov.common.R
import ru.bespalov.common.view.SiBackgroundImage
import ru.bespalov.common.view.SiTextButton
import ru.bespalov.common.view.SiTextField
import ru.bespalov.domain.model.User
import ru.bespalov.navigation.ScreenNavigation

@Composable
fun RegistrationScreen(
    navController: NavController,
    state: RegistrationState,
    sendEvent: (RegistrationEvent) -> Unit
) {
    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        SiBackgroundImage(drawableResId = R.drawable.si_backgroud)

        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center
        ) {
            SiTextField(
                value = state.email,
                label = "Email: ",
                onValueChange = { sendEvent(RegistrationEvent.OnEmailChanged(it)) }
            )
            SiTextField(
                value = state.nickname,
                label = "Nickname: ",
                onValueChange = { sendEvent(RegistrationEvent.OnNicknameChanged(it)) }
            )
            SiTextField(
                value = state.password,
                label = "Password: ",
                onValueChange = { RegistrationEvent.OnPasswordChanged(it) }
            )
            if (state.email.isNotEmpty() && state.password.isNotEmpty() && state.nickname.isNotEmpty()) {
                SiTextButton(buttonTextResourceId = R.string.registration_button) {
                    sendEvent(
                        RegistrationEvent.RegistrationButtonClicked(
                            user = User(state.email, state.password, state.nickname),
                            onSuccess = {
                                navController.navigate(ScreenNavigation.MainMenuScreen.routeName)
                            }
                        )
                    )
                }
            }
        }
    }
}