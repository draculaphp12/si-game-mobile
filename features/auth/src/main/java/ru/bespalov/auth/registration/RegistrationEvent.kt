package ru.bespalov.auth.registration

import ru.bespalov.common.mvi.UiEvent
import ru.bespalov.domain.model.User

sealed class RegistrationEvent: UiEvent {
    class RegistrationButtonClicked(
        val user: User,
        val onSuccess: () -> Unit,
    ): RegistrationEvent()
    class OnEmailChanged(val email: String): RegistrationEvent()
    class OnPasswordChanged(val password: String): RegistrationEvent()
    class OnNicknameChanged(val nickname: String): RegistrationEvent()
}
