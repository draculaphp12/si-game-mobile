package ru.bespalov.auth.sign_in

import ru.bespalov.common.mvi.UiEvent
import ru.bespalov.domain.model.User

sealed class SignInEvent : UiEvent {
    class SignInEventButtonClicked(
        val user: User,
        val onSuccess: () -> Unit,
    ): SignInEvent()
    class OnEmailChanged(val email: String): SignInEvent()
    class OnPasswordChanged(val password: String): SignInEvent()
}