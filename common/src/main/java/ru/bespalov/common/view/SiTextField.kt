package ru.bespalov.common.view

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import ru.bespalov.common.R

@Composable
fun SiTextField(
    value: String,
    label: String,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    onValueChange: (String) -> Unit,
) {
    TextField(
        keyboardOptions = keyboardOptions,
        value = value,
        onValueChange = onValueChange,
        label = {
            Text(
                label,
                color = Color.White
            )
        },
        modifier = Modifier
            .padding(vertical = dimensionResource(id = R.dimen.padding_8dp))
            .border(
                BorderStroke(dimensionResource(id = R.dimen.border_1dp), Color.White)
            )
            .fillMaxWidth(),
        colors = TextFieldDefaults.textFieldColors(textColor = Color.White)
    )
}