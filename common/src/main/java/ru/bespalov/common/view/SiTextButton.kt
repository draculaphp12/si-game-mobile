package ru.bespalov.common.view

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import ru.bespalov.common.R

@Composable
fun SiTextButton(buttonTextResourceId: Int, onClick: () -> Unit) {
    TextButton(
        onClick = onClick,
        modifier = Modifier
            .padding(vertical = dimensionResource(id = R.dimen.padding_8dp))
            .border(
                BorderStroke(1.dp, Color.White)
            )
            .fillMaxWidth(),
    ) {
        Text(
            text = stringResource(id = buttonTextResourceId).uppercase(),
            modifier = Modifier.padding(8.dp),
            color = Color.White
        )
    }
}