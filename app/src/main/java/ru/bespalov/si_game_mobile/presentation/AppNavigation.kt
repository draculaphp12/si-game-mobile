package ru.bespalov.si_game_mobile.presentation

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import org.koin.androidx.compose.getKoin
import org.koin.androidx.compose.getViewModel
import org.koin.androidx.compose.koinViewModel
import org.koin.core.parameter.parametersOf
import ru.bespalov.auth.auth.AuthScreen
import ru.bespalov.auth.registration.RegistrationScreen
import ru.bespalov.auth.registration.RegistrationState
import ru.bespalov.auth.registration.RegistrationViewModel
import ru.bespalov.auth.sign_in.SignInScreen
import ru.bespalov.auth.sign_in.SignInState
import ru.bespalov.auth.sign_in.SignInViewModel
import ru.bespalov.connect_game.ConnectToGameScreen
import ru.bespalov.connect_game.ConnectToGameViewModel
import ru.bespalov.create_host.CreateHostScreen
import ru.bespalov.create_host.CreateHostState
import ru.bespalov.create_host.CreateHostViewModel
import ru.bespalov.domain.repository.AuthRepository
import ru.bespalov.game.GameScreen
import ru.bespalov.game.GameState
import ru.bespalov.game.GameViewModel
import ru.bespalov.main_menu.MainMenu
import ru.bespalov.navigation.ScreenNavigation
import ru.bespalov.question.QuestionScreen
import ru.bespalov.question.QuestionState
import ru.bespalov.question.QuestionViewModel
import ru.bespalov.waiting_room.WaitingRoomScreen
import ru.bespalov.waiting_room.WaitingRoomViewModel

@Composable
fun AppNavigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = ScreenNavigation.AuthScreen.routeName) {
        composable(ScreenNavigation.AuthScreen.routeName) {
            val authRepository: AuthRepository = getKoin().get()
            AuthScreen(navController, authRepository.getCurrentUserId())
        }
        composable(ScreenNavigation.SignInScreen.routeName) {
            val signInViewModel = koinViewModel<SignInViewModel>()
            val state by signInViewModel.state.collectAsState(initial = SignInState.initialState)
            SignInScreen(navController, state, signInViewModel::sendEvent)
        }
        composable(ScreenNavigation.RegistrationScreen.routeName) {
            val registrationViewModel = koinViewModel<RegistrationViewModel>()
            val state by registrationViewModel.state.collectAsState(initial = RegistrationState.initialState)
            RegistrationScreen(navController, state, registrationViewModel::sendEvent)
        }
        composable(ScreenNavigation.MainMenuScreen.routeName) { MainMenu(navController) }
        composable(ScreenNavigation.ConnectToGameScreen.routeName) {
            val connectToGameViewModel = koinViewModel<ConnectToGameViewModel>()
            ConnectToGameScreen(
                navController,
                connectToGameViewModel.gameId,
                connectToGameViewModel::connectPlayer
            )
        }
        composable(ScreenNavigation.CreateHostScreen.routeName) {
            val createHostViewModel = koinViewModel<CreateHostViewModel>()
            val state by createHostViewModel.state.collectAsState(initial = CreateHostState.initialState)
            CreateHostScreen(
                navController,
                createHostViewModel::sendEvent,
                state
            )
        }
        composable(
            ScreenNavigation.GameScreen.routeName + "/{gameId}",
            arguments = listOf(
                navArgument("gameId") { type = NavType.StringType }
            )
        ) {
            BackHandler(true) {}
            val gameViewModel = getViewModel<GameViewModel> {
                parametersOf(
                    it.arguments?.getString("gameId"),
                    navController
                )
            }
            val state by gameViewModel.state.collectAsState(initial = GameState.initialState)
            GameScreen(
                it.arguments?.getString("gameId"),
                state,
                gameViewModel::sendEvent
            )
        }
        composable(
            ScreenNavigation.WaitingRoomScreen.routeName + "?gameId={gameId}",
            arguments = listOf(
                navArgument("gameId") { type = NavType.StringType },
            )
        ) {
            val waitingRoomViewModel = getViewModel<WaitingRoomViewModel> {
                parametersOf(
                    it.arguments?.getString("gameId"),
                )
            }
            WaitingRoomScreen(
                navController,
                it.arguments?.getString("gameId"),
                waitingRoomViewModel.playersList
            )
        }
        composable(
            ScreenNavigation.QuestionScreen.routeName + "?questionId={questionId}&gameId={gameId}",
            arguments = listOf(
                navArgument("questionId") { type = NavType.StringType },
                navArgument("gameId") { type = NavType.StringType }
            )
        ) {
            BackHandler(true) {}
            val questionViewModel = getViewModel<QuestionViewModel> {
                parametersOf(
                    it.arguments?.getString("questionId")
                )
            }
            val state by questionViewModel.state.collectAsState(initial = QuestionState.initialState)
            QuestionScreen(
                it.arguments?.getString("questionId"),
                it.arguments?.getString("gameId"),
                navController,
                questionViewModel::sendEvent,
                state
            )
        }
    }
}