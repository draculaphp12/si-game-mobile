package ru.bespalov.si_game_mobile.di

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import ru.bespalov.data.di.databaseModule
import ru.bespalov.data.di.domainModule

class SiGameApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@SiGameApp)
            modules(databaseModule, domainModule, androidModule)
        }
    }
}