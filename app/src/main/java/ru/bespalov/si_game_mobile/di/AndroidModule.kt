package ru.bespalov.si_game_mobile.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.bespalov.auth.registration.RegistrationViewModel
import ru.bespalov.auth.sign_in.SignInViewModel
import ru.bespalov.create_host.CreateHostViewModel
import ru.bespalov.game.GameViewModel
import ru.bespalov.question.QuestionViewModel
import ru.bespalov.waiting_room.WaitingRoomViewModel

val androidModule = module {
    viewModel {
        ru.bespalov.connect_game.ConnectToGameViewModel(
            connectPlayerUseCase = get()
        )
    }

    viewModel {
        CreateHostViewModel(
            createGameWithPlayerUseCase = get()
        )
    }

    viewModel { params ->
        GameViewModel(
            params.get(),
            getGameFlowUseCase = get(),
            getRoundUseCase = get(),
            getPlayersUseCase = get(),
            updateCurrentQuestionUseCase = get(),
            navController = params.get(),
            checkAnswersUseCase = get(),
            getQuestionUseCase = get(),
            getPlayerAnswerUseCase = get(),
            increaseCurrentRoundUseCase = get(),
            acceptDisputeUseCase = get(),
            openDisputeUseCase = get(),
            closeDisputeUseCase = get()
        )
    }

    viewModel { params ->
        QuestionViewModel(
            questionId = params.get(),
            getQuestionUseCase = get(),
            updateCurrentRespondentUseCase = get(),
            getGameFlowUseCase = get(),
            checkPlayerFalseAnswerUseCase = get(),
            updateLastQuestionIdUseCase = get(),
            rightAnswerUseCase = get(),
            falseAnswerUseCase = get(),
            answerTimeOutUseCase = get()
        )
    }

    viewModel { params ->
        WaitingRoomViewModel(
            gameId = params.get(),
            initGameRoomUseCase = get()
        )
    }

    viewModel {
        RegistrationViewModel(
            authRepository = get()
        )
    }

    viewModel {
        SignInViewModel(
            authRepository = get()
        )
    }
}