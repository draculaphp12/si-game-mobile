plugins {
    id("com.android.application")
    kotlin("android")
    id("com.google.gms.google-services")
}

android {
    compileSdk = 33

    defaultConfig {
        applicationId = "ru.bespalov.si_game_mobile"
        minSdk = 21
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.3.1"
    }
    packagingOptions {
        resources.excludes.add("/META-INF/{AL2.0,LGPL2.1}")
    }
}

dependencies {
    implementation(project(":domain"))
    implementation(project(":data"))
    implementation(project(":common"))
    implementation(project(":navigation"))
    implementation(project(":features:create_host"))
    implementation(project(":features:connect_game"))
    implementation(project(":features:waiting_room"))
    implementation(project(":features:game"))
    implementation(project(":features:question"))
    implementation(project(":features:auth"))
    implementation(project(":features:main_menu"))

    implementation(Dependencies.Androidx.activityCompose)
    implementation(Dependencies.Androidx.core)
    implementation(Dependencies.Androidx.navigationCompose)
    implementation(Dependencies.Androidx.legacySupportV4)
    implementation(Dependencies.Androidx.lifecycleRuntimeKtx)
    implementation(Dependencies.Androidx.lifecycleViewModelCompose)

    implementation(Dependencies.Androidx.Compose.ui)
    implementation(Dependencies.Androidx.Compose.material)
    implementation(Dependencies.Androidx.Compose.toolingPreview)

    implementation(Dependencies.Koin.core)
    implementation(Dependencies.Koin.android)
    implementation(Dependencies.Koin.androidxCompose)

    debugImplementation(Dependencies.debugImpl)
}