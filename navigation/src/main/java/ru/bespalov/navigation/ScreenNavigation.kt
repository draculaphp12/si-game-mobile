package ru.bespalov.navigation

sealed class ScreenNavigation(var routeName: String){
    object MainMenuScreen : ScreenNavigation(routeName = "mainMenu")
    object ConnectToGameScreen : ScreenNavigation(routeName = "connectToGame")
    object CreateHostScreen : ScreenNavigation(routeName = "createHost")
    object WaitingRoomScreen : ScreenNavigation(routeName = "waitingRoomHost")
    object GameScreen : ScreenNavigation(routeName = "game")
    object QuestionScreen : ScreenNavigation(routeName = "question")
    object AuthScreen : ScreenNavigation(routeName = "auth")
    object SignInScreen : ScreenNavigation(routeName = "signIn")
    object RegistrationScreen : ScreenNavigation(routeName = "registration")
}
