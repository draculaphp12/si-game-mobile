import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

open class CommonAndroidPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.configurePlugins()
        project.configureAndroid()
        project.configureDependencies()
    }
}

internal fun Project.configureAndroid() = extensions.getByType(com.android.build.gradle.LibraryExtension::class.java).run {
    compileSdk = 33
    defaultConfig {
        minSdk = 21
        targetSdk = 33
    }
    buildTypes {
        release {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.3.1"
    }
}

internal fun Project.configureDependencies() = dependencies {
    add("implementation", Dependencies.Androidx.navigationCompose)
    add("implementation", Dependencies.Androidx.core)
    add("implementation", Dependencies.Androidx.Compose.ui)
    add("implementation", Dependencies.Androidx.Compose.material)
    add("implementation", Dependencies.Androidx.Compose.toolingPreview)
    add("implementation", Dependencies.Androidx.lifecycleRuntimeKtx)
    add("implementation", Dependencies.Androidx.activityCompose)
    add("implementation", Dependencies.Androidx.legacySupportV4)
    add("implementation", Dependencies.Androidx.lifecycleViewModelCompose)

    add("implementation", project(":domain"))
    add("implementation", project(":data"))
    add("implementation", project(":common"))
    add("implementation", project(":navigation"))
}

internal fun Project.configurePlugins() {
    plugins.apply("com.android.library")
    plugins.apply("org.jetbrains.kotlin.android")
}

