@file:Suppress("PackageDirectoryMismatch")

object Versions {
    object Androidx {
        const val compose = "1.2.0"
        const val activityCompose = "1.6.1"
        const val core = "1.9.0"
        const val navigationCompose = "2.5.3"
        const val legacySupportV4 = "1.0.0"
        const val lifecycleRuntimeKtx = "2.5.1"
        const val lifecycleViewModelCompose = "2.5.1"
    }

    object Koin {
        const val core = "3.2.2"
        const val android = "3.3.1"
        const val androidxCompose = "3.4.0"
    }

    const val room = "2.5.0"
    const val firebaseBom = "30.5.0"
    const val exoPlayer = "2.18.2"
    const val coroutinesPlayServices = "1.6.4"
    const val coilCompose = "2.2.2"

    object Tests {
        const val junit = "4.13.2"
        const val androidxJunit = "1.1.5"
        const val espressoCore = "3.5.1"
    }
}