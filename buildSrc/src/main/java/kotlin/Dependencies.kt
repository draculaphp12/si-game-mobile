@file:Suppress("PackageDirectoryMismatch")

object Dependencies {
    object Androidx {
        object Compose {
            const val ui = "androidx.compose.ui:ui:${Versions.Androidx.compose}"
            const val material = "androidx.compose.material:material:${Versions.Androidx.compose}"
            const val toolingPreview = "androidx.compose.ui:ui-tooling-preview:${Versions.Androidx.compose}"
        }

        const val activityCompose = "androidx.activity:activity-compose:${Versions.Androidx.activityCompose}"
        const val core = "androidx.core:core-ktx:${Versions.Androidx.core}"
        const val navigationCompose = "androidx.navigation:navigation-compose:${Versions.Androidx.navigationCompose}"
        const val legacySupportV4 = "androidx.legacy:legacy-support-v4:${Versions.Androidx.legacySupportV4}"
        const val lifecycleRuntimeKtx = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.Androidx.lifecycleRuntimeKtx}"
        const val lifecycleViewModelCompose = "androidx.lifecycle:lifecycle-viewmodel-compose:${Versions.Androidx.lifecycleViewModelCompose}"
    }

    object Koin {
        const val core = "io.insert-koin:koin-core:${Versions.Koin.core}"
        const val android = "io.insert-koin:koin-android:${Versions.Koin.android}"
        const val androidxCompose = "io.insert-koin:koin-androidx-compose:${Versions.Koin.androidxCompose}"
    }

    object Room {
        const val runtime = "androidx.room:room-runtime:${Versions.room}"
        const val roomKtx = "androidx.room:room-ktx:${Versions.room}"
        const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
    }

    object Firebase {
        const val bom = "com.google.firebase:firebase-bom:${Versions.firebaseBom}"
        const val firestoreKtx = "com.google.firebase:firebase-firestore-ktx"
        const val storageKtx = "com.google.firebase:firebase-storage-ktx"
        const val authKtx = "com.google.firebase:firebase-auth-ktx"
    }

    const val exoPlayer = "com.google.android.exoplayer:exoplayer:${Versions.exoPlayer}"
    const val coroutinesPlayServices = "org.jetbrains.kotlinx:kotlinx-coroutines-play-services:${Versions.coroutinesPlayServices}"
    const val coilCompose = "io.coil-kt:coil-compose:${Versions.coilCompose}"

    object Tests {
        const val junit = "junit:junit:${Versions.Tests.junit}"
        const val androidxJunit = "androidx.test.ext:junit:${Versions.Tests.androidxJunit}"
        const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.Tests.espressoCore}"
        const val composeUiJunit4 = "androidx.compose.ui:ui-test-junit4:${Versions.Androidx.compose}"
    }

    const val debugImpl = "androidx.compose.ui:ui-tooling:${Versions.Androidx.compose}"
}