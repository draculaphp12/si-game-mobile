package ru.bespalov.data.di

import androidx.room.Room
import org.koin.dsl.module
import ru.bespalov.data.db.AppDatabase

val databaseModule = module {
    single {
        Room.databaseBuilder(get(), AppDatabase::class.java, "SiGameDB")
            .build()
    }

    single {
        get<AppDatabase>().getAnswerDao()
    }

    single {
        get<AppDatabase>().getDisputeDao()
    }

    single {
        get<AppDatabase>().getGameDao()
    }

    single {
        get<AppDatabase>().getPackDao()
    }

    single {
        get<AppDatabase>().getPlayerDao()
    }

    single {
        get<AppDatabase>().getQuestionDao()
    }

    single {
        get<AppDatabase>().getRoundDao()
    }

    single {
        get<AppDatabase>().getThemeDao()
    }
}