package ru.bespalov.data.di

import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import org.koin.dsl.module
import ru.bespalov.data.db.repository.AnswerRepositoryImpl
import ru.bespalov.data.db.repository.AuthRepositoryImpl
import ru.bespalov.data.db.repository.DisputeRepositoryImpl
import ru.bespalov.data.db.repository.GameRepositoryImpl
import ru.bespalov.data.db.repository.PackRepositoryImpl
import ru.bespalov.data.db.repository.PlayerRepositoryImpl
import ru.bespalov.data.db.repository.QuestionRepositoryImpl
import ru.bespalov.data.db.repository.RoundRepositoryImpl
import ru.bespalov.domain.repository.AnswerRepository
import ru.bespalov.domain.repository.AuthRepository
import ru.bespalov.domain.repository.DisputeRepository
import ru.bespalov.domain.repository.GameRepository
import ru.bespalov.domain.repository.PackRepository
import ru.bespalov.domain.repository.PlayerRepository
import ru.bespalov.domain.repository.QuestionRepository
import ru.bespalov.domain.repository.RoundRepository
import ru.bespalov.domain.usecase.ConnectPlayerUseCase
import ru.bespalov.domain.usecase.CreateGameWithPlayerUseCase
import ru.bespalov.domain.usecase.GetGameFlowUseCase
import ru.bespalov.domain.usecase.GetPlayersUseCase
import ru.bespalov.domain.usecase.GetQuestionUseCase
import ru.bespalov.domain.usecase.GetRoundUseCase
import ru.bespalov.domain.usecase.IncreaseCurrentRoundUseCase
import ru.bespalov.domain.usecase.InitGameRoomUseCase
import ru.bespalov.domain.usecase.UpdateCurrentQuestionUseCase
import ru.bespalov.domain.usecase.UpdateCurrentRespondentUseCase
import ru.bespalov.domain.usecase.UpdateLastQuestionIdUseCase
import ru.bespalov.domain.usecase.answer.AnswerTimeOutUseCase
import ru.bespalov.domain.usecase.answer.CheckAnswersUseCase
import ru.bespalov.domain.usecase.answer.CheckPlayerFalseAnswerUseCase
import ru.bespalov.domain.usecase.answer.FalseAnswerUseCase
import ru.bespalov.domain.usecase.answer.GetPlayerAnswerUseCase
import ru.bespalov.domain.usecase.answer.RightAnswerUseCase
import ru.bespalov.domain.usecase.dispute.AcceptDisputeUseCase
import ru.bespalov.domain.usecase.dispute.CloseDisputeUseCase
import ru.bespalov.domain.usecase.dispute.OpenDisputeUseCase

val domainModule = module {
    single<QuestionRepository> {
        QuestionRepositoryImpl(
            questionDao = get()
        )
    }

    single<AnswerRepository> {
        AnswerRepositoryImpl(
            questionRepository = get(),
            answerDao = get()
        )
    }

    single<DisputeRepository> {
        DisputeRepositoryImpl(
            disputeDao = get()
        )
    }

    single<RoundRepository> {
        RoundRepositoryImpl(
            roundDao = get()
        )
    }

    single<GameRepository> {
        GameRepositoryImpl(
            gameDao = get()
        )
    }

    single<PackRepository> {
        PackRepositoryImpl(
            packDao = get(),
            roundDao = get(),
            themeDao = get(),
            questionDao = get()
        )
    }

    single<PlayerRepository> {
        PlayerRepositoryImpl(
            playerDao = get()
        )
    }

    single<AuthRepository> {
        AuthRepositoryImpl(
            auth = get()
        )
    }

    single {
        Firebase.auth
    }

    factory { CheckAnswersUseCase(answerRepository = get()) }

    factory { CheckPlayerFalseAnswerUseCase(answerRepository = get()) }

    factory { ConnectPlayerUseCase(playerRepository = get()) }

    factory { CreateGameWithPlayerUseCase(gameRepository = get()) }

    factory { GetGameFlowUseCase(gameRepository = get()) }

    factory { GetPlayerAnswerUseCase(answerRepository = get()) }

    factory { GetPlayersUseCase(playerRepository = get()) }

    factory { GetQuestionUseCase(questionRepository = get()) }

    factory { GetRoundUseCase(roundRepository = get()) }

    factory { IncreaseCurrentRoundUseCase(gameRepository = get()) }

    factory { UpdateCurrentQuestionUseCase(gameRepository = get()) }

    factory { UpdateCurrentRespondentUseCase(gameRepository = get()) }

    factory { UpdateLastQuestionIdUseCase(gameRepository = get()) }

    factory { OpenDisputeUseCase(gameRepository = get(), disputeRepository = get()) }

    factory { CloseDisputeUseCase(gameRepository = get(), disputeRepository = get()) }

    factory {
        AcceptDisputeUseCase(
            gameRepository = get(),
            disputeRepository = get(),
            playerRepository = get()
        )
    }

    factory {
        InitGameRoomUseCase(
            gameRepository = get(),
            playerRepository = get(),
            packRepository = get()
        )
    }

    factory {
        RightAnswerUseCase(
            gameRepository = get(),
            playerRepository = get(),
            answerRepository = get()
        )
    }

    factory {
        FalseAnswerUseCase(
            gameRepository = get(),
            playerRepository = get(),
            answerRepository = get()
        )
    }

    factory {
        AnswerTimeOutUseCase(
            gameRepository = get(),
            answerRepository = get()
        )
    }
}