package ru.bespalov.data.model.firebase

import ru.bespalov.data.db.model.AnswerEntity
import ru.bespalov.domain.model.Answer

internal data class AnswerNode(
    val questionId: String = "",
    val playerId: String? = "",
    val playerAnswer: String = ""
)

internal fun AnswerNode.toAnswerEntity(isRight: Boolean): AnswerEntity? = playerId?.let {
    AnswerEntity(
        questionId = questionId,
        playerId = it,
        playerAnswer = playerAnswer,
        isRight = isRight
    )
}

internal fun Answer.toNode(): AnswerNode = AnswerNode(
    questionId, playerId, playerAnswer
)