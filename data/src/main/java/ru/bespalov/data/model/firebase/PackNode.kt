package ru.bespalov.data.model.firebase

import ru.bespalov.domain.model.Pack

data class PackNode(
    var gameId: String = "",
    val name: String = "",
    val rounds: List<RoundNode> = emptyList()
)

fun PackNode.toPack(): Pack {
    val rounds = rounds.map { it.toRound() }
    return Pack(gameId = gameId, name = name, rounds)
}

fun Pack.toPackNode(): PackNode {
    return PackNode(
        gameId = gameId,
        name = name,
        rounds = rounds.map { it.toRoundNode() }
    )
}
