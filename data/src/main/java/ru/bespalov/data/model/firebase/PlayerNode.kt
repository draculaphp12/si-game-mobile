package ru.bespalov.data.model.firebase

import ru.bespalov.data.db.model.PlayerEntity

data class PlayerNode(
    val uuid: String = "",
    val nickname: String = "",
    val score: Int = 0,
    @field:JvmField
    val isModerator: Boolean = false
)

internal fun PlayerNode.toPlayerEntity(gameId: String): PlayerEntity = PlayerEntity(
    uuid = uuid,
    gameId = gameId,
    nickname =  nickname,
    score = score,
    isModerator = isModerator
)