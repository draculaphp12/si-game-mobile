package ru.bespalov.data.model


import ru.bespalov.data.model.firebase.ContentNode
import ru.bespalov.data.model.firebase.PackNode
import ru.bespalov.data.model.firebase.ParamNode
import ru.bespalov.data.model.firebase.QuestionNode
import ru.bespalov.data.model.firebase.RoundNode
import ru.bespalov.data.model.firebase.ThemeNode
import java.util.*

internal data class PackageTag(
    val name: String,
    val id: String,
    val roundsTag: RoundsTag?
)

internal data class RoundsTag(
    val rounds: List<RoundTag>
)

internal data class RoundTag(
    val name: String,
    val themesTag: ThemesTag?
)

internal data class ThemesTag(
    val themes: List<ThemeTag>
)

internal data class ThemeTag(
    val name: String,
    val questionsTag: QuestionsTag?
)

internal data class QuestionsTag(
    val questions: List<QuestionTag>
)

internal data class TypeTag(
    val name: String,
    val param: List<ParamTag>
)

internal data class ParamTag(
    val name: String,
    val text: String
)

internal data class QuestionTag(
    val price: Int,
    val scenarioTag: ScenarioTag?,
    val rightTag: RightTag?,
    val type: TypeTag?
)

internal data class ScenarioTag(
    val atom: List<AtomTag>
)

internal data class AtomTag(
    // Если type = null, значит это текст
    val type: String?,
    val text: String
)

internal data class RightTag(
    val answer: String
)

internal fun PackageTag.mapToPackNode(): PackNode {
    var roundNumber = 0
    val rounds = roundsTag!!.rounds.map {
        roundNumber++
        it.mapToRoundNode(id, roundNumber)
    }
    return PackNode(
        gameId = id,
        name = name,
        rounds = rounds
    )
}

internal fun RoundTag.mapToRoundNode(packId: String, roundNumber: Int): RoundNode {
    val roundId = UUID.randomUUID().toString()
    val themes = themesTag?.themes?.map {
        it.mapToThemeNode(roundId)
    }
    return RoundNode(
        id = roundId,
        packId = packId,
        name = name,
        themes = themes ?: emptyList(),
        roundNumber
    )
}

internal fun ThemeTag.mapToThemeNode(roundId: String): ThemeNode {
    val themeId = UUID.randomUUID().toString()
    val questions = questionsTag?.questions?.map {
        it.mapToQuestionNode(themeId)
    }
    return ThemeNode(
        id = themeId,
        roundId = roundId,
        name = name,
        questions = questions ?: emptyList()
    )
}

internal fun QuestionTag.mapToQuestionNode(themeId: String): QuestionNode {
    val questionId = UUID.randomUUID().toString()
    val content = scenarioTag?.atom?.map {
        it.mapToContentNode(questionId)
    }
    val param = type?.param?.map {
        it.mapToParamNode(questionId)
    }
    return QuestionNode(
        id = questionId,
        themeId = themeId,
        price = price,
        rightAnswer = rightTag?.answer,
        type = type?.name ?: "simple",
        content = content,
        param = param
    )
}

internal fun AtomTag.mapToContentNode(questionId: String): ContentNode {
    return ContentNode(
        questionId = questionId,
        type = type?: "text",
        value = text.replace("@", "")
    )
}

internal fun ParamTag.mapToParamNode(questionId: String): ParamNode {
    return ParamNode(
        questionId = questionId,
        name = name,
        value = text
    )
}