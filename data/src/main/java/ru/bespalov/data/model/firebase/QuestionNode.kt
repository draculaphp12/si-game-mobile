package ru.bespalov.data.model.firebase

import ru.bespalov.domain.model.Question
import ru.bespalov.domain.model.QuestionContent


data class QuestionNode(
    val id: String? = "",
    val themeId: String? = "",
    val price: Int? = 0,
    val rightAnswer: String? = "",
    val type: String? = "simple",
    val content: List<ContentNode>? = emptyList(),
    val param: List<ParamNode>? = emptyList()
)

data class ContentNode(
    val questionId: String? = "",
    val type: String? = "text",
    val value: String? = ""
)

data class ParamNode(
    val questionId: String? = "",
    val name: String? = "",
    val value: String? = ""
)

fun ContentNode.toQuestionContent(): QuestionContent {
    with(this) {
        when (type) {
            "image" -> {
                return QuestionContent.ImageQuestionContent(value?: "")
            }
            "video" -> {
                return QuestionContent.VideoQuestionContent(value?: "")
            }
            "voice" -> {
                return QuestionContent.AudioQuestionContent(value?: "")
            }
            "say" -> {
                return QuestionContent.SayQuestionContent(value?:"")
            }
            else -> {
                return QuestionContent.TextQuestionContent(value?: "")
            }
        }
    }
}

fun QuestionContent.toContentNode(questionId: String): ContentNode {
    with(this) {
        when (this) {
            is QuestionContent.ImageQuestionContent -> {
                return ContentNode(
                    questionId = questionId,
                    type = "image",
                    value = content
                )
            }
            is QuestionContent.VideoQuestionContent -> {
                return ContentNode(
                    questionId = questionId,
                    type = "video",
                    value = content
                )
            }
            is QuestionContent.AudioQuestionContent -> {
                return ContentNode(
                    questionId = questionId,
                    type = "voice",
                    value = content
                )
            }
            is QuestionContent.SayQuestionContent -> {
                return ContentNode(
                    questionId = questionId,
                    type = "say",
                    value = content
                )
            }
            is QuestionContent.TextQuestionContent -> {
                return ContentNode(
                    questionId = questionId,
                    type = "text",
                    value = content
                )
            }
        }
    }
}

fun QuestionNode.toQuestion(): Question {
    val questionContent = content!!.map {
        it.toQuestionContent()
    }
    return Question(id.toString(), price!!, rightAnswer!!, questionContent, isChecked = false)
}

fun Question.toQuestionNode(themeId: String): QuestionNode {
    val params = mutableListOf<ParamNode>()
    return QuestionNode(
        id = id,
        themeId = themeId,
        price = price,
        rightAnswer = rightAnswer,
        type = "simple",
        content = content.map { it.toContentNode(id) },
        param = params
    )
}