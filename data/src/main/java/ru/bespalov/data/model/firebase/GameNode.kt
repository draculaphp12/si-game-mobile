package ru.bespalov.data.model.firebase

import ru.bespalov.domain.model.Game

internal data class GameNode(
    val id: String = "",
    val currentPlayerId: String = "",
    val roundsCount: Int = 1,
    val currentRound: Int = 1,
    val maxPlayersCount: Int = 5,
    var currentQuestionId: String? = null,
    var currentRespondentId: String? = null,
    var lastQuestionId: String? = null,
    @field:JvmField
    var isDisputeOpen: Boolean? = false,
    var currentDisputantId: String? = null
)

internal fun GameNode.toGame(): Game = Game(
    id,
    currentPlayerId,
    roundsCount,
    currentRound,
    maxPlayersCount,
    currentQuestionId,
    currentRespondentId,
    lastQuestionId,
    isDisputeOpen,
    currentDisputantId
)
