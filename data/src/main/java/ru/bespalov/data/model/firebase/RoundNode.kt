package ru.bespalov.data.model.firebase

import ru.bespalov.domain.model.Round

data class RoundNode(
    val id: String = "",
    val packId: String? = "",
    val name: String = "",
    val themes: List<ThemeNode> = emptyList(),
    val number: Int = 1
)

fun RoundNode.toRound(): Round {
    val themes = themes.map { it.toTheme() }
    return Round(id, name = name, themes, number)
}

fun Round.toRoundNode(): RoundNode {
    return RoundNode(
        id = id,
        name = name,
        themes = themes.map { it.toThemeNode(id) },
        number = number
    )
}
