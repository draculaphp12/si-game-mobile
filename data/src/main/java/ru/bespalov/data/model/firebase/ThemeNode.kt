package ru.bespalov.data.model.firebase

import ru.bespalov.domain.model.Theme

data class ThemeNode(
    val id: String = "",
    val roundId: String? = "",
    val name: String = "",
    val questions: List<QuestionNode> = emptyList()
)

fun ThemeNode.toTheme(): Theme {
    val questions = questions.map { it.toQuestion() }
    return Theme(id, name = name, questions)
}

fun Theme.toThemeNode(roundId: String): ThemeNode {
    val questions = questions.map { it.toQuestionNode(id) }
    return ThemeNode(
        id = id,
        name = name,
        roundId = roundId,
        questions = questions
    )
}
