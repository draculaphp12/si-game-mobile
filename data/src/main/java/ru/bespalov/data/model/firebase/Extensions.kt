package ru.bespalov.data.model.firebase

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow


fun CollectionReference.observeData(): Flow<QuerySnapshot?> {
    return callbackFlow {
        val listener = EventListener<QuerySnapshot> { snapshot, error ->
            if (error != null) { close(error.cause) }
            trySend(snapshot)
        }
        val registration = addSnapshotListener(listener)
        awaitClose { registration.remove() }
    }
}

fun DocumentReference.observeData(): Flow<DocumentSnapshot?> {
    return callbackFlow {
        val listener = EventListener<DocumentSnapshot> { snapshot, error ->
            if (error != null) { close(error.cause) }
            trySend(snapshot)
        }
        val registration = addSnapshotListener(listener)
        awaitClose { registration.remove() }
    }
}