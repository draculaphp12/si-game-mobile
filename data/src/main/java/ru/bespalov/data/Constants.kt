package ru.bespalov.data

import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

const val GAME_COLLECTION = "games"
const val PLAYERS_COLLECTION = "players"
const val PACKS_COLLECTION = "packs"
const val ANSWERS_COLLECTION = "answers"
const val FALSE_ANSWERS_COLLECTION = "false_answers"

const val LOG_TAG = "SI_TAG"

const val IMAGES_DIR = "Images"
const val VIDEO_DIR = "Video"
const val AUDIO_DIR = "Audio"

enum class GameField(val fieldName: String) {
    CURRENT_QUESTION_ID("currentQuestionId"),
    CURRENT_RESPONDENT_ID("currentRespondentId"),
    LAST_QUESTION_ID("lastQuestionId"),
    IS_DISPUTE_OPEN("isDisputeOpen"),
    CURRENT_DISPUTANT_ID("currentDisputantId"),
    CURRENT_ROUND("currentRound"),
}

val currentUserId = Firebase.auth.currentUser!!.uid