package ru.bespalov.data

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Xml
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import ru.bespalov.data.model.AtomTag
import ru.bespalov.data.model.PackageTag
import ru.bespalov.data.model.ParamTag
import ru.bespalov.data.model.QuestionTag
import ru.bespalov.data.model.QuestionsTag
import ru.bespalov.data.model.RightTag
import ru.bespalov.data.model.RoundTag
import ru.bespalov.data.model.RoundsTag
import ru.bespalov.data.model.ScenarioTag
import ru.bespalov.data.model.ThemeTag
import ru.bespalov.data.model.ThemesTag
import ru.bespalov.data.model.TypeTag
import ru.bespalov.data.model.firebase.PackNode
import ru.bespalov.data.model.mapToPackNode
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.net.URLDecoder
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream


object SiPackParser {

    val progressFlow = MutableSharedFlow<Float>()

    private val storage = Firebase.storage
    private val storageRef = storage.reference
    private val imagesList = mutableListOf<String>()
    private val audiosList = mutableListOf<String>()
    private val videosList = mutableListOf<String>()
    private var completedTasks = 0
    private var filesCount = 0

    fun parseZip(context: Context, fileUri: Uri, coroutineScope: CoroutineScope): PackNode? {
        // for init loading progress bar before loading files
        coroutineScope.launch {
            progressFlow.emit(0.01f)
        }
        val contentResolver = context.contentResolver
        var pack: PackageTag? = null

        val zipFileName = queryFileInfo(contentResolver, fileUri, OpenableColumns.DISPLAY_NAME)
        val zipFileSize = queryFileInfo(contentResolver, fileUri, OpenableColumns.SIZE).toInt()

        if (zipFileSize > 104857600) {
            throw IllegalArgumentException("Размер файла не должен превышать 100 МБ!")
        }

        if (!zipFileName.contains(".siq")) {
            throw IllegalArgumentException("Загружаемый файл должен иметь формат типа .siq!")
        }

        val newDirectoryName = zipFileName.substring(0, zipFileName.indexOf("."))
        val storageRepository = StorageRepository(context)

        val audioDirectory = storageRepository
            .getPackStorageDir("$newDirectoryName/$AUDIO_DIR").absolutePath
        val videoDirectory = storageRepository
            .getPackStorageDir("$newDirectoryName/$VIDEO_DIR").absolutePath
        val imageDirectory = storageRepository
            .getPackStorageDir("$newDirectoryName/$IMAGES_DIR").absolutePath

        try {
            ZipInputStream(contentResolver.openInputStream(fileUri)).use { zin ->
                var entry: ZipEntry?
                var name: String
                while (zin.nextEntry.also { entry = it } != null) {
                    name = entry?.name ?: ""
                    when {
                        name.startsWith("$IMAGES_DIR/") -> unzipFile(name, imageDirectory, zin, imagesList)
                        name.startsWith("$AUDIO_DIR/") -> unzipFile(name, audioDirectory, zin, audiosList)
                        name.startsWith("$VIDEO_DIR/") -> unzipFile(name, videoDirectory, zin, videosList)
                        name == "content.xml" -> {
                            pack = parseSiPack(zin)
                        }
                    }
                }
            }
            filesCount = imagesList.size + audiosList.size + videosList.size
            uploadFiles(imageDirectory, imagesList, coroutineScope, newDirectoryName, IMAGES_DIR)
            uploadFiles(audioDirectory, audiosList, coroutineScope, newDirectoryName, AUDIO_DIR)
            uploadFiles(videoDirectory, videosList, coroutineScope, newDirectoryName, VIDEO_DIR)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return pack?.mapToPackNode()
    }

    private fun queryFileInfo(resolver: ContentResolver, uri: Uri, fieldName: String): String {
        val returnCursor: Cursor = resolver.query(uri, null, null, null, null)!!
        val fieldIndex: Int = returnCursor.getColumnIndex(fieldName)
        returnCursor.moveToFirst()
        val field: String = returnCursor.getString(fieldIndex)
        returnCursor.close()
        return field
    }

    private fun uploadFiles(
        fileDirectory: String,
        filesList: MutableList<String>,
        coroutineScope: CoroutineScope,
        newDirectoryName: String,
        firebaseDirectory: String
    ) {
        filesList.forEach { fileName ->
            val file = File("$fileDirectory/$fileName")
            val fileRef = storageRef.child("$newDirectoryName/$firebaseDirectory/$fileName")
            fileRef.putStream(FileInputStream(file)).addOnCompleteListener {
                file.delete()
                completedTasks++
                val progress: Float = completedTasks / filesCount.toFloat()
                coroutineScope.launch {
                    progressFlow.emit(progress)
                }
            }
        }
    }

    private fun unzipFile(
        name: String,
        fileDirectory: String,
        zin: ZipInputStream,
        fileList: MutableList<String>
    ) {
        var fileName = name.substring(name.indexOf("/") + 1, name.length)
        fileName = URLDecoder.decode(fileName, "utf-8")
        val file = File("$fileDirectory/$fileName")
        file.outputStream().use { output ->
            zin.copyTo(output)
        }
        fileList.add(fileName)
    }

    private enum class SiTag {
        PACKAGE, ROUNDS, ROUND, THEMES, THEME, QUESTIONS, QUESTION, SCENARIO, RIGHT, TYPE, ATOM,
        ANSWER, PARAM;
        override fun toString(): String {
            return name.lowercase(Locale.getDefault())
        }
        enum class Attribute {
            NAME, ID, PRICE, TYPE;
            override fun toString(): String {
                return name.lowercase(Locale.getDefault())
            }
        }
    }

    private fun parseSiPack(inputStream: InputStream): PackageTag {
        val packageTag: PackageTag?
        val parser: XmlPullParser = Xml.newPullParser()
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
        parser.setInput(inputStream, null)
        parser.nextTag()
        packageTag = parsePackageTag(parser)
        return packageTag
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parsePackageTag(parser: XmlPullParser): PackageTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.PACKAGE.toString())

        val name: String = parser.getAttributeValue(null, SiTag.Attribute.NAME.toString())
        val id: String = parser.getAttributeValue(null, SiTag.Attribute.ID.toString())

        var rounds: RoundsTag? = null
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.ROUNDS.toString()) {
                rounds = parseRoundsTag(parser)
            } else {
                skip(parser)
            }
        }


        return PackageTag(name, id, rounds)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseRoundsTag(parser: XmlPullParser): RoundsTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.ROUNDS.toString())
        val rounds = mutableListOf<RoundTag>()
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.ROUND.toString()) {
                rounds.add(parseRoundTag(parser))
            } else {
                skip(parser)
            }
        }
        return RoundsTag(rounds)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseRoundTag(parser: XmlPullParser): RoundTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.ROUND.toString())
        val name = parser.getAttributeValue(null, SiTag.Attribute.NAME.toString())
        var themes: ThemesTag? = null
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.THEMES.toString()) {
                themes = parseThemesTag(parser)
            } else {
                skip(parser)
            }
        }
        return RoundTag(name, themes)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseThemesTag(parser: XmlPullParser): ThemesTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.THEMES.toString())
        val themes: MutableList<ThemeTag> = mutableListOf()
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.THEME.toString()) {
                themes.add(parseThemeTag(parser))
            } else {
                skip(parser)
            }
        }
        return ThemesTag(themes)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseThemeTag(parser: XmlPullParser): ThemeTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.THEME.toString())
        val name = parser.getAttributeValue(null, SiTag.Attribute.NAME.toString())
        var questionsTag: QuestionsTag? = null
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.QUESTIONS.toString()) {
                questionsTag = parseQuestionsTag(parser)
            } else {
                skip(parser)
            }
        }
        return ThemeTag(name, questionsTag)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseQuestionsTag(parser: XmlPullParser): QuestionsTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.QUESTIONS.toString())
        val questions: MutableList<QuestionTag> = mutableListOf()
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.QUESTION.toString()) {
                questions.add(parseQuestionTag(parser))
            } else {
                skip(parser)
            }
        }
        return QuestionsTag(questions)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseQuestionTag(parser: XmlPullParser): QuestionTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.QUESTION.toString())
        val price = parser.getAttributeValue(null, SiTag.Attribute.PRICE.toString()).toInt()
        var scenarioTag: ScenarioTag? = null
        var rightTag: RightTag? = null
        var typeTag: TypeTag? = null
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                SiTag.SCENARIO.toString() -> {
                    scenarioTag = parseScenarioTag(parser)
                }
                SiTag.RIGHT.toString() -> {
                    rightTag = parseRightTag(parser)
                }
                SiTag.TYPE.toString() -> {
                    typeTag = parseTypeTag(parser)
                }
                else -> {
                    skip(parser)
                }
            }
        }
        return QuestionTag(price, scenarioTag, rightTag, typeTag)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseScenarioTag(parser: XmlPullParser): ScenarioTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.SCENARIO.toString())
        val atomTagList: MutableList<AtomTag> = mutableListOf()
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.ATOM.toString()) {
                atomTagList.add(parseAtomTag(parser))
            } else {
                skip(parser)
            }
        }
        return ScenarioTag(atomTagList)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseAtomTag(parser: XmlPullParser): AtomTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.ATOM.toString())
        val type = parser.getAttributeValue(null, SiTag.Attribute.TYPE.toString())
        val text = readText(parser)
        return AtomTag(type, text)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseRightTag(parser: XmlPullParser): RightTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.RIGHT.toString())
        var answer = ""
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.ANSWER.toString()) {
                answer = readText(parser)
            } else {
                skip(parser)
            }
        }
        return RightTag(answer)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseTypeTag(parser: XmlPullParser): TypeTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.TYPE.toString())
        val name = parser.getAttributeValue(null, SiTag.Attribute.NAME.toString())
        val param: MutableList<ParamTag> = mutableListOf()
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == SiTag.PARAM.toString()) {
                param.add(parseParamTag(parser))
            } else {
                skip(parser)
            }
        }
        return TypeTag(name, param)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun parseParamTag(parser: XmlPullParser): ParamTag {
        parser.require(XmlPullParser.START_TAG, null, SiTag.PARAM.toString())
        val name = parser.getAttributeValue(null, SiTag.Attribute.NAME.toString())
        val text = readText(parser)
        return ParamTag(name, text)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readText(parser: XmlPullParser): String {
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return result
    }
}