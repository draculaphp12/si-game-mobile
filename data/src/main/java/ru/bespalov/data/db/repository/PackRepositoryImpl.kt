package ru.bespalov.data.db.repository

import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import ru.bespalov.data.AUDIO_DIR
import ru.bespalov.data.GAME_COLLECTION
import ru.bespalov.data.IMAGES_DIR
import ru.bespalov.data.LOG_TAG
import ru.bespalov.data.PACKS_COLLECTION
import ru.bespalov.data.VIDEO_DIR
import ru.bespalov.data.db.dao.PackDao
import ru.bespalov.data.db.dao.QuestionDao
import ru.bespalov.data.db.dao.RoundDao
import ru.bespalov.data.db.dao.ThemeDao
import ru.bespalov.data.db.model.QuestionContentTuple
import ru.bespalov.data.db.model.QuestionTuple
import ru.bespalov.data.db.model.toPackEntity
import ru.bespalov.data.model.firebase.PackNode
import ru.bespalov.data.model.firebase.toPack
import ru.bespalov.domain.model.Pack
import ru.bespalov.domain.model.QuestionContent
import ru.bespalov.domain.repository.PackRepository
import java.net.URLDecoder

internal class PackRepositoryImpl(
    private val packDao: PackDao,
    private val roundDao: RoundDao,
    private val themeDao: ThemeDao,
    private val questionDao: QuestionDao
): PackRepository {
    private val db = Firebase.firestore

    private suspend fun savePack(pack: Pack) {
        try {
            val isPackNotExists = packDao.addPack(pack.toPackEntity()) != -1L
            if (isPackNotExists) {
                var roundNumber = 1
                pack.rounds.forEach { round ->
                    roundDao.addRound(round.id, round.name, pack.gameId, roundNumber)
                    roundNumber++
                    round.themes.forEach { theme ->
                        themeDao.addTheme(theme.id, theme.name, round.id)
                        theme.questions.forEach { question ->
                            val content = mutableListOf<QuestionContentTuple>()
                            val questionTuple =
                                QuestionTuple(question.price, question.rightAnswer, "simple")
                            question.content.forEach { questionContent ->
                                when (questionContent) {
                                    is QuestionContent.AudioQuestionContent -> {
                                        val name =
                                            URLDecoder.decode(questionContent.content, "utf-8")
                                        content.add(
                                            QuestionContentTuple(
                                                "voice",
                                                "${pack.name}/${AUDIO_DIR}/${name}"
                                            )
                                        )
                                    }
                                    is QuestionContent.VideoQuestionContent -> {
                                        val name =
                                            URLDecoder.decode(questionContent.content, "utf-8")
                                        content.add(
                                            QuestionContentTuple(
                                                "video",
                                                "${pack.name}/${VIDEO_DIR}/${name}"
                                            )
                                        )
                                    }
                                    is QuestionContent.ImageQuestionContent -> {
                                        val name =
                                            URLDecoder.decode(questionContent.content, "utf-8")
                                        content.add(
                                            QuestionContentTuple(
                                                "image",
                                                "${pack.name}/${IMAGES_DIR}/${name}"
                                            )
                                        )
                                    }
                                    is QuestionContent.TextQuestionContent -> {
                                        content.add(
                                            QuestionContentTuple(
                                                "text",
                                                questionContent.content
                                            )
                                        )
                                    }
                                    is QuestionContent.SayQuestionContent -> {
                                        content.add(
                                            QuestionContentTuple(
                                                "say",
                                                questionContent.content
                                            )
                                        )
                                    }
                                }
                            }
                            questionDao.addQuestion(
                                question.id,
                                theme.id,
                                questionTuple,
                                content,
                                false
                            )
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }

    override suspend fun cachePack(gameId: String) {
        val packDocument = db
            .collection(GAME_COLLECTION).document(gameId)
            .collection(PACKS_COLLECTION).document(gameId)
            .get()

        val pack = packDocument.await().toObject(PackNode::class.java)?.toPack()
        if (pack != null) {
            savePack(pack)
        }
    }
}