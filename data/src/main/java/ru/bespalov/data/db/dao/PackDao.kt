package ru.bespalov.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.IGNORE
import ru.bespalov.data.db.model.PackEntity

@Dao
internal interface PackDao {
    @Insert(onConflict = IGNORE)
    suspend fun addPack(pack: PackEntity): Long
}