package ru.bespalov.data.db.repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import ru.bespalov.domain.model.User
import ru.bespalov.domain.repository.AuthRepository

class AuthRepositoryImpl(
    private val auth: FirebaseAuth
) : AuthRepository {
    override fun registration(
        user: User,
        onSuccess: () -> Unit,
        onFailure: (String) -> Unit
    ) {
        auth.createUserWithEmailAndPassword(user.email, user.password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val profileUpdates = userProfileChangeRequest {
                        displayName = user.nickname
                    }
                    auth.currentUser!!.updateProfile(profileUpdates)
                    onSuccess()
                } else {
                    onFailure(task.exception?.message.toString())
                }
            }
    }

    override fun signIn(
        user: User,
        onSuccess: () -> Unit,
        onFailure: (String) -> Unit
    ) {
        auth.signInWithEmailAndPassword(user.email, user.password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    onSuccess()
                } else {
                    onFailure(task.exception?.message.toString())
                }
            }
    }

    override fun getCurrentUserId(): String? {
        return auth.currentUser?.uid
    }
}