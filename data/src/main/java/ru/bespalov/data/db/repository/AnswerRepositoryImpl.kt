package ru.bespalov.data.db.repository

import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import ru.bespalov.data.ANSWERS_COLLECTION
import ru.bespalov.data.FALSE_ANSWERS_COLLECTION
import ru.bespalov.data.GAME_COLLECTION
import ru.bespalov.data.db.dao.AnswerDao
import ru.bespalov.data.db.model.toEntity
import ru.bespalov.data.model.firebase.AnswerNode
import ru.bespalov.data.model.firebase.observeData
import ru.bespalov.data.model.firebase.toAnswerEntity
import ru.bespalov.data.model.firebase.toNode
import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.repository.AnswerRepository
import ru.bespalov.domain.repository.QuestionRepository

internal class AnswerRepositoryImpl(
    private val questionRepository: QuestionRepository,
    private val answerDao: AnswerDao
): AnswerRepository {
    private val db = Firebase.firestore

    override suspend fun addAnswerToFirebase(
        gameId: String,
        answer: Answer
    ) {
        try {
            db
                .collection(GAME_COLLECTION)
                .document(gameId)
                .collection(ANSWERS_COLLECTION)
                .document(answer.questionId)
                .set(answer.toNode())
                .await()

            answerDao.insertAnswer(answer.toEntity())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override suspend fun addFalseAnswer(
        gameId: String,
        answer: Answer,
        onSuccess: () -> Unit
    ) {
        try {
            db
                .collection(GAME_COLLECTION)
                .document(gameId)
                .collection(FALSE_ANSWERS_COLLECTION)
                .document()
                .set(answer.toNode())
                .await()
            answerDao.insertAnswer(answer.toEntity())
            onSuccess.invoke()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override suspend fun checkPlayerFalseAnswer(questionId: String, playerId: String): Boolean {
        var isAnswered = false
        try {
            isAnswered = answerDao.getAnswer(questionId, playerId, 0) != null
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return isAnswered
    }

    override suspend fun checkAnswers(gameId: String) {
        try {
            db
                .collection(GAME_COLLECTION)
                .document(gameId)
                .collection(ANSWERS_COLLECTION)
                .get()
                .await()
                .documents
                .forEach {
                    val answer = it.toObject(AnswerNode::class.java)
                    answer?.let { questionRepository.updateQuestion(answer.questionId) }
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override suspend fun observeAnswers(gameId: String) {
        try {
            db
                .collection(GAME_COLLECTION)
                .document(gameId)
                .collection(ANSWERS_COLLECTION)
                .observeData()
                .collect { snapshot ->
                    for (dc in snapshot!!.documentChanges) {
                        val answer = dc.document.toObject(AnswerNode::class.java)
                        if (dc.type == DocumentChange.Type.ADDED) {
                            questionRepository.updateQuestion(answer.questionId)
                            answer.toAnswerEntity(true)?.let {
                                answerDao.insertAnswer(it)
                            }
                        }
                    }
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override suspend fun getAnswer(questionId: String, playerId: String?, isRight: Int): Answer? {
        var answerEntity: Answer? = null
        try {
            answerEntity = if (playerId != null) {
                answerDao.getAnswer(questionId, playerId, isRight)?.toAnswer()
            } else {
                answerDao.getRightAnswer(questionId)?.toAnswer()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return answerEntity
    }
}