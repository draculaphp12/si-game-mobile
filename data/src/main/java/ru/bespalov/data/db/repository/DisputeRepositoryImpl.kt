package ru.bespalov.data.db.repository

import ru.bespalov.data.db.dao.DisputeDao
import ru.bespalov.data.db.model.toEntity
import ru.bespalov.domain.model.Dispute
import ru.bespalov.domain.repository.DisputeRepository

internal class DisputeRepositoryImpl(
    private val disputeDao: DisputeDao
): DisputeRepository {
    override suspend fun addDispute(dispute: Dispute) {
        try {
            disputeDao.addDispute(dispute.toEntity())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override suspend fun checkIfDisputeExist(questionId: String, playerId: String): Boolean {
        var isDisputeExist = false
        try {
            isDisputeExist = disputeDao.getDispute(questionId, playerId) != null
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return isDisputeExist
    }
}