package ru.bespalov.data.db.repository

import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.tasks.await
import ru.bespalov.data.db.dao.QuestionDao
import ru.bespalov.data.db.model.toQuestion
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.model.QuestionContent.*
import ru.bespalov.domain.repository.QuestionRepository

internal class QuestionRepositoryImpl(
    private val questionDao: QuestionDao
): QuestionRepository {
    private val storage = Firebase.storage

    override suspend fun getQuestion(questionId: String): Question? {
        val questionListFromDb = questionDao.getQuestion(questionId)
        if (questionListFromDb.isNotEmpty()) {
            val question = questionListFromDb.first().toQuestion()
            question.content.forEach { content ->
                val fileName: String
                when (content) {
                    is AudioQuestionContent, is VideoQuestionContent, is ImageQuestionContent -> {
                        fileName = content.content
                        val uri = storage.reference.child(fileName).downloadUrl.await().toString()
                        content.content = uri
                    }
                    else -> {}
                }
            }
            return question
        }
        return null
    }

    override suspend fun updateQuestion(questionId: String) {
        try {
            questionDao.updateQuestionCheckedState(questionId)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}