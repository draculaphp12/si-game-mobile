package ru.bespalov.data.db.repository

import android.util.Log
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.tasks.await
import ru.bespalov.data.GAME_COLLECTION
import ru.bespalov.data.GameField
import ru.bespalov.data.LOG_TAG
import ru.bespalov.data.PACKS_COLLECTION
import ru.bespalov.data.PLAYERS_COLLECTION
import ru.bespalov.data.db.dao.GameDao
import ru.bespalov.data.db.model.toGame
import ru.bespalov.data.db.model.toGameEntity
import ru.bespalov.data.model.firebase.GameNode
import ru.bespalov.data.model.firebase.PlayerNode
import ru.bespalov.data.model.firebase.observeData
import ru.bespalov.data.model.firebase.toGame
import ru.bespalov.data.model.firebase.toPackNode
import ru.bespalov.domain.model.Game
import ru.bespalov.domain.model.Pack
import ru.bespalov.domain.repository.GameRepository
import java.util.*

internal class GameRepositoryImpl(
    private val gameDao: GameDao
): GameRepository {
    private val db = Firebase.firestore

    override fun createGameWithPlayer(maxPlayersCount: Int, roundsCount: Int, pack: Pack): String {
        val currentPlayer = Firebase.auth.currentUser!!
        val gameID = UUID.randomUUID().toString()
        val packNode = pack.toPackNode()
        packNode.gameId = gameID

        val player = PlayerNode(
            uuid = currentPlayer.uid,
            nickname = currentPlayer.displayName ?: "User $currentPlayer.uid",
            score = 0,
            isModerator = true
        )

        val game = GameNode(
            id = gameID,
            currentPlayerId = currentPlayer.uid,
            roundsCount = roundsCount,
            currentRound = 1,
            maxPlayersCount = maxPlayersCount,
        )

        db.collection(GAME_COLLECTION)
            .document(gameID).set(game)
            .addOnSuccessListener {
                db
                    .collection(GAME_COLLECTION).document(gameID)
                    .collection(PLAYERS_COLLECTION).document(player.uuid).set(player)

                db
                    .collection(GAME_COLLECTION).document(gameID)
                    .collection(PACKS_COLLECTION).document(gameID).set(packNode)
            }

        return gameID
    }

    override suspend fun cacheGame(gameId: String) {
        val gameDocument = db
            .collection(GAME_COLLECTION)
            .document(gameId)

        val game = gameDocument.get().await().toObject(GameNode::class.java)?.toGame()
        if (game != null) {
            saveGame(game)
        }
    }

    private suspend fun saveGame(game: Game) {
        try {
            gameDao.addGame(game.toGameEntity())
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }

    override fun getGameFlowFromDB(gameId: String): Flow<Game?> {
        try {
            return gameDao.getGame(gameId).map { it?.toGame() }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
        return flowOf()
    }

    override suspend fun observeGame(gameId: String) {
        try {
            db
                .collection(GAME_COLLECTION)
                .document(gameId)
                .observeData()
                .collect { snapshot ->
                    val game = snapshot?.toObject(GameNode::class.java)?.toGame()
                    if (game != null) {
                        saveGame(game)
                    }
                }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }

    override suspend fun updateCurrentQuestion(gameId: String, questionId: String?) {
        updateGameField(gameId, questionId, GameField.CURRENT_QUESTION_ID.fieldName)
    }

    override suspend fun updateCurrentRespondent(gameId: String, playerId: String?) {
        updateGameField(gameId, playerId, GameField.CURRENT_RESPONDENT_ID.fieldName)
    }

    override fun updateLastQuestionId(gameId: String, lastQuestionId: String) {
        try {
            db
                .collection(GAME_COLLECTION)
                .document(gameId)
                .update(GameField.LAST_QUESTION_ID.fieldName, lastQuestionId)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }

    override fun updateIsDisputeOpen(gameId: String, isDisputeOpen: Boolean) {
        try {
            db
                .collection(GAME_COLLECTION)
                .document(gameId)
                .get()
                .addOnSuccessListener { snapshot ->
                    if (snapshot!!.exists()) {
                        val game = snapshot.toObject(GameNode::class.java)
                        if (
                            game !== null
                            && !snapshot.metadata.hasPendingWrites()
                        ) {
                            if (
                                game.isDisputeOpen == false && isDisputeOpen ||
                                game.isDisputeOpen == true && !isDisputeOpen
                            ) {
                                db
                                    .collection(GAME_COLLECTION)
                                    .document(gameId)
                                    .update(GameField.IS_DISPUTE_OPEN.fieldName, isDisputeOpen)
                            }
                        }
                    }
                }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }

    override suspend fun updateCurrentDisputantId(gameId: String, playerId: String?) {
        updateGameField(gameId, playerId, GameField.CURRENT_DISPUTANT_ID.fieldName)
    }

    private suspend fun updateGameField(gameId: String, newFieldValue: String?, fieldName: String) {
        val game = db
            .collection(GAME_COLLECTION)
            .document(gameId)
            .get()
            .await()
            .toObject(GameNode::class.java)

        val oldFieldValue: String?
        if (game !== null) {
            oldFieldValue = when (fieldName) {
                GameField.CURRENT_DISPUTANT_ID.fieldName -> game.currentDisputantId
                GameField.CURRENT_RESPONDENT_ID.fieldName -> game.currentRespondentId
                GameField.CURRENT_QUESTION_ID.fieldName -> game.currentQuestionId
                else -> null
            }
            if (
                oldFieldValue == null && newFieldValue != null ||
                oldFieldValue != null && newFieldValue == null
            ) {
                db
                    .collection(GAME_COLLECTION)
                    .document(gameId)
                    .update(fieldName, newFieldValue)
            }
        }
    }

    override suspend fun increaseCurrentRound(gameId: String) {
        try {
            val game = db
                .collection(GAME_COLLECTION)
                .document(gameId)
                .get()
                .await()
                .toObject(GameNode::class.java)
            if (game !== null) {
                db
                    .collection(GAME_COLLECTION)
                    .document(gameId)
                    .update(GameField.CURRENT_ROUND.fieldName, FieldValue.increment(1L))
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }
}