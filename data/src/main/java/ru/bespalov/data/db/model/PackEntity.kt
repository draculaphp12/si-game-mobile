package ru.bespalov.data.db.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import ru.bespalov.domain.model.Pack

@Entity(
    tableName = "packs"
)
internal data class PackEntity(
    @PrimaryKey
    val gameId: String,
    val name: String,
)

internal data class PackWithRounds(
    @Embedded val pack: PackEntity,
    @Relation(
        parentColumn = "gameId",
        entityColumn = "gameId",
        entity = RoundEntity::class
    )
    val rounds: List<RoundWithThemes>
)

internal fun Pack.toPackEntity(): PackEntity = PackEntity(
    gameId, name
)