package ru.bespalov.data.db.dao

import androidx.room.Dao
import androidx.room.Query

@Dao
internal interface ThemeDao {
    @Query("INSERT OR REPLACE INTO themes (id, name, roundId) VALUES (:id, :name, :roundId)")
    suspend fun addTheme(id: String, name: String, roundId: String)
}