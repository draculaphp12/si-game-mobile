package ru.bespalov.data.db.repository

import android.util.Log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import ru.bespalov.data.LOG_TAG
import ru.bespalov.data.db.dao.RoundDao
import ru.bespalov.data.db.model.toRound
import ru.bespalov.domain.model.Round
import ru.bespalov.domain.repository.RoundRepository

internal class RoundRepositoryImpl(
    private val roundDao: RoundDao
): RoundRepository {
    override fun getRoundFlow(roundNumber: Int, gameId: String): Flow<List<Round>> {
        try {
            return roundDao.getRound(roundNumber, gameId).map { list ->
                list.map { it.toRound() }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
        return flowOf()
    }
}