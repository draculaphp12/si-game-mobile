package ru.bespalov.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import ru.bespalov.data.db.model.AnswerEntity

@Dao
internal interface AnswerDao {
    @Query("SELECT * FROM answers WHERE questionId = :questionId AND playerId = :playerId AND isRight = :isRight")
    suspend fun getAnswer(questionId: String, playerId: String, isRight: Int): AnswerEntity?

    @Insert(onConflict = REPLACE)
    suspend fun insertAnswer(answerEntity: AnswerEntity)

    @Query("SELECT * FROM answers WHERE questionId = :questionId AND isRight = 1")
    suspend fun getRightAnswer(questionId: String): AnswerEntity?
}