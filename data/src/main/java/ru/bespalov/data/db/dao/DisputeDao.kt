package ru.bespalov.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import ru.bespalov.data.db.model.DisputeEntity

@Dao
internal interface DisputeDao {
    @Query("SELECT * FROM disputes WHERE questionId = :questionId AND playerId = :playerId")
    suspend fun getDispute(questionId: String, playerId: String): DisputeEntity?

    @Insert(onConflict = REPLACE)
    suspend fun addDispute(disputeEntity: DisputeEntity)
}