package ru.bespalov.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.bespalov.data.db.model.GameEntity

@Dao
internal interface GameDao {
    @Insert(onConflict = REPLACE)
    suspend fun addGame(gameEntity: GameEntity)

    @Query("SELECT * FROM games WHERE id = :gameId")
    fun getGame(gameId: String): Flow<GameEntity?>
}