package ru.bespalov.data.db.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import ru.bespalov.domain.model.Round

@Entity(
    tableName = "rounds"
)
internal data class RoundEntity(
    @PrimaryKey
    val id: String,
    val gameId: String,
    val name: String,
    val number: Int
)

internal data class RoundWithThemes(
    @Embedded val round: RoundEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "roundId",
        entity = ThemeEntity::class
    )
    val themes: List<ThemeWithQuestions>
)

internal fun RoundWithThemes.toRound(): Round {
    return Round(round.id, round.name, themes.map { it.toTheme() }, round.number)
}