package ru.bespalov.data.db.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import ru.bespalov.domain.model.Theme

@Entity(
    tableName = "themes"
)
internal data class ThemeEntity(
    @PrimaryKey
    val id: String,
    val roundId: String,
    val name: String
)

internal data class ThemeWithQuestions(
    @Embedded val theme: ThemeEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "themeId",
        entity = QuestionEntity::class
    )
    val questions: List<QuestionWithContent>
)

internal fun ThemeWithQuestions.toTheme(): Theme {
    val questions = questions.map { it.toQuestion() }
    return Theme(theme.id, theme.name, questions)
}
