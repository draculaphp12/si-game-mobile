package ru.bespalov.data.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.bespalov.data.db.model.PlayerEntity

@Dao
internal interface PlayerDao {
    @Insert(onConflict = REPLACE)
    suspend fun addPlayer(playerEntity: PlayerEntity)

    @Insert(onConflict = REPLACE)
    suspend fun addPlayersList(playerList: List<PlayerEntity>)

    @Query("SELECT * FROM players WHERE gameId = :gameId")
    fun getGamePlayersList(gameId: String): Flow<List<PlayerEntity>>

    @Delete
    suspend fun deletePlayer(playerEntity: PlayerEntity)

    @Query("UPDATE players SET score = :score WHERE uuid = :playerId")
    suspend fun updatePlayerScore(playerId: String, score: Int)
}