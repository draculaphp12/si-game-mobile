package ru.bespalov.data.db.model

import androidx.room.Entity
import ru.bespalov.domain.model.Answer

@Entity(
    tableName = "answers",
    primaryKeys = ["questionId", "playerId", "isRight"]
)
internal data class AnswerEntity(
    val questionId: String,
    val playerId: String,
    val playerAnswer: String,
    val isRight: Boolean
) {
    fun toAnswer(): Answer = Answer(
        questionId, playerId, playerAnswer, isRight
    )
}

internal fun Answer.toEntity(): AnswerEntity = AnswerEntity(
    questionId, playerId ?: "-", playerAnswer, isRight
)