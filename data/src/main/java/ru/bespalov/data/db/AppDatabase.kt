package ru.bespalov.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.bespalov.data.db.dao.AnswerDao
import ru.bespalov.data.db.dao.DisputeDao
import ru.bespalov.data.db.dao.GameDao
import ru.bespalov.data.db.dao.PackDao
import ru.bespalov.data.db.dao.PlayerDao
import ru.bespalov.data.db.dao.QuestionDao
import ru.bespalov.data.db.dao.RoundDao
import ru.bespalov.data.db.dao.ThemeDao
import ru.bespalov.data.db.model.AnswerEntity
import ru.bespalov.data.db.model.DisputeEntity
import ru.bespalov.data.db.model.GameEntity
import ru.bespalov.data.db.model.PackEntity
import ru.bespalov.data.db.model.PlayerEntity
import ru.bespalov.data.db.model.QuestionContentEntity
import ru.bespalov.data.db.model.QuestionEntity
import ru.bespalov.data.db.model.RoundEntity
import ru.bespalov.data.db.model.ThemeEntity

@Database(
    version = 1,
    entities = [
        PackEntity::class,
        RoundEntity::class,
        ThemeEntity::class,
        QuestionEntity::class,
        QuestionContentEntity::class,
        GameEntity::class,
        PlayerEntity::class,
        AnswerEntity::class,
        DisputeEntity::class
    ]
)
internal abstract class AppDatabase : RoomDatabase() {
    abstract fun getPackDao(): PackDao
    abstract fun getRoundDao(): RoundDao
    abstract fun getThemeDao(): ThemeDao
    abstract fun getQuestionDao(): QuestionDao
    abstract fun getGameDao(): GameDao
    abstract fun getPlayerDao(): PlayerDao
    abstract fun getAnswerDao(): AnswerDao
    abstract fun getDisputeDao(): DisputeDao
}