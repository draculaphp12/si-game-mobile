package ru.bespalov.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.bespalov.domain.model.Game

@Entity(
    tableName = "games"
)
internal data class GameEntity(
    @PrimaryKey
    val id: String,
    val currentPlayerId: String,
    val roundsCount: Int = 1,
    val currentRound: Int = 1,
    val maxPlayersCount: Int = 5,
    var currentQuestionId: String? = null,
    var currentRespondentId: String? = null,
    var lastQuestionId: String? = null,
    var isDisputeOpen: Boolean? = false,
    var currentDisputantId: String? = null
)

internal fun GameEntity.toGame(): Game = Game(
    id,
    currentPlayerId = currentPlayerId,
    roundsCount = roundsCount,
    currentRound = currentRound,
    maxPlayersCount = maxPlayersCount,
    currentQuestionId = currentQuestionId,
    currentRespondentId = currentRespondentId,
    lastQuestionId = lastQuestionId,
    isDisputeOpen = isDisputeOpen,
    currentDisputantId = currentDisputantId
)

internal fun Game.toGameEntity(): GameEntity = GameEntity(
    id = id,
    currentPlayerId = currentPlayerId,
    roundsCount = roundsCount,
    currentRound = currentRound,
    maxPlayersCount = maxPlayersCount,
    currentQuestionId = currentQuestionId,
    currentRespondentId = currentRespondentId,
    lastQuestionId = lastQuestionId,
    isDisputeOpen = isDisputeOpen,
    currentDisputantId = currentDisputantId
)