package ru.bespalov.data.db.model

import androidx.room.Entity
import ru.bespalov.domain.model.Dispute

@Entity(
    tableName = "disputes",
    primaryKeys = ["questionId", "playerId"]
)
internal data class DisputeEntity(
    val questionId: String,
    val playerId: String,
    val result: Boolean
) {
    fun toDispute(): Dispute = Dispute(
        questionId, playerId, result
    )
}

internal fun Dispute.toEntity(): DisputeEntity = DisputeEntity(
    questionId, playerId, result
)