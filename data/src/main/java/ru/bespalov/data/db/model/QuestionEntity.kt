package ru.bespalov.data.db.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.model.QuestionContent

@Entity(
    tableName = "questions"
)
internal data class QuestionEntity(
    @PrimaryKey
    val id: String,
    val themeId: String,
    val price: Int,
    val rightAnswer: String,
    val type: String,
    val isChecked: Boolean = false
)

internal data class QuestionTuple(
    val price: Int,
    val rightAnswer: String,
    val type: String
)

internal fun QuestionWithContent.toQuestion(): Question {
    with(this.question) {
        val questionContent = content.mapNotNull {
            it.toQuestionContent()
        }
        return Question(id, price, rightAnswer, questionContent, isChecked)
    }
}

@Entity(
    tableName = "questions_content",
    primaryKeys = ["questionId", "type"]
)
internal data class QuestionContentEntity(
    val questionId: String,
    val type: String,
    val value: String
)

internal data class QuestionContentTuple(
    val type: String,
    val value: String
)

internal fun QuestionContentEntity.toQuestionContent(): QuestionContent? {
    with(this) {
        when(type) {
            "text" -> {
                return QuestionContent.TextQuestionContent(value)
            }
            "image" -> {
                return QuestionContent.ImageQuestionContent(value)
            }
            "video" -> {
                return QuestionContent.VideoQuestionContent(value)
            }
            "voice" -> {
                return QuestionContent.AudioQuestionContent(value)
            }
            "say" -> {
                return QuestionContent.SayQuestionContent(value)
            }
            else -> {
                return null
            }
        }
    }
}

internal data class QuestionWithContent(
    @Embedded val question: QuestionEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "questionId"
    )
    val content: List<QuestionContentEntity>
)
