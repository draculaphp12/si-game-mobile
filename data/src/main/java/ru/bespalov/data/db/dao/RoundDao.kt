package ru.bespalov.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow
import ru.bespalov.data.db.model.RoundWithThemes

@Dao
internal interface RoundDao {
    @Transaction
    @Query("SELECT * FROM rounds WHERE number = :roundNumber AND gameId = :gameId")
    fun getRound(roundNumber: Int, gameId: String): Flow<List<RoundWithThemes>>

    @Query("INSERT OR REPLACE INTO rounds (id, name, gameId, number) VALUES (:id,:name, :packId, :number)")
    suspend fun addRound(id: String, name: String, packId: String, number: Int)
}