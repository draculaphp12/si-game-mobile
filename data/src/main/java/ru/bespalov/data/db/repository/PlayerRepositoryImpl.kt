package ru.bespalov.data.db.repository

import android.util.Log
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.tasks.await
import ru.bespalov.data.GAME_COLLECTION
import ru.bespalov.data.LOG_TAG
import ru.bespalov.data.PLAYERS_COLLECTION
import ru.bespalov.data.db.dao.PlayerDao
import ru.bespalov.data.db.model.PlayerEntity
import ru.bespalov.data.db.model.toPlayer
import ru.bespalov.data.model.firebase.PlayerNode
import ru.bespalov.data.model.firebase.observeData
import ru.bespalov.data.model.firebase.toPlayerEntity
import ru.bespalov.domain.model.Player
import ru.bespalov.domain.repository.PlayerRepository

internal class PlayerRepositoryImpl(
    private val playerDao: PlayerDao
): PlayerRepository {
    private val db = Firebase.firestore

    override suspend fun cachePlayersList(gameId: String) {
        val playersList = db
            .collection(GAME_COLLECTION)
            .document(gameId)
            .collection(PLAYERS_COLLECTION)
            .get()
            .await()
            .documents
            .mapNotNull { document ->
                document.toObject<PlayerNode>()?.toPlayerEntity(gameId)
            }
        savePlayersList(playersList)
    }

    override fun getGamePlayersFromDB(gameId: String): Flow<List<Player>> {
        try {
            return playerDao.getGamePlayersList(gameId).map { list ->
                list.map { it.toPlayer() }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
        return flowOf()
    }

    override suspend fun observeGamePlayersList(gameId: String) {
        db
            .collection(GAME_COLLECTION)
            .document(gameId)
            .collection(PLAYERS_COLLECTION)
            .observeData()
            .collect { snapshot ->
                for (dc in snapshot!!.documentChanges) {
                    val playerEntity = dc.document.toObject<PlayerNode>().toPlayerEntity(gameId)
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            savePlayer(playerEntity)
                        }
                        DocumentChange.Type.MODIFIED -> {
                            updatePlayerScore(playerEntity.uuid, playerEntity.score)
                        }
                        DocumentChange.Type.REMOVED -> {
                            deletePlayer(playerEntity)
                        }
                        else -> {}
                    }
                }
            }
    }

    private suspend fun deletePlayer(playerEntity: PlayerEntity) {
        try {
            playerDao.deletePlayer(playerEntity)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }

    private suspend fun savePlayer(playerEntity: PlayerEntity) {
        try {
            playerDao.addPlayer(playerEntity)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }

    override fun connectPlayer(gameId: String) {
        val currentPlayer = Firebase.auth.currentUser!!
        val db = Firebase.firestore
        val player = PlayerNode(
            uuid = currentPlayer.uid,
            nickname = currentPlayer.displayName ?: "User $currentPlayer.uid",
            score = 0,
            isModerator = false
        )

        db
            .collection(GAME_COLLECTION)
            .document(gameId)
            .collection(PLAYERS_COLLECTION)
            .document(player.uuid)
            .get()
            .addOnSuccessListener { snapshot ->
                if (!snapshot.exists()) {
                    db
                        .collection(GAME_COLLECTION)
                        .document(gameId)
                        .collection(PLAYERS_COLLECTION)
                        .document(player.uuid)
                        .set(player)
                }
            }
    }

    private suspend fun savePlayersList(playersList: List<PlayerEntity>) {
        try {
            playerDao.addPlayersList(playersList)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(LOG_TAG, e.message.toString())
        }
    }

    private suspend fun updatePlayerScore(playerId: String, score: Int) {
        try {
            playerDao.updatePlayerScore(playerId, score)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun updatePlayerScoreFirebase(playerId: String, score: Int, gameId: String) {
        try {
            db
                .collection(GAME_COLLECTION).document(gameId)
                .collection(PLAYERS_COLLECTION).document(playerId)
                .update("score", FieldValue.increment(score.toLong()))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}