package ru.bespalov.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import ru.bespalov.data.db.model.QuestionContentTuple
import ru.bespalov.data.db.model.QuestionTuple
import ru.bespalov.data.db.model.QuestionWithContent

@Dao
internal abstract class QuestionDao {
    @Transaction
    @Query("SELECT * FROM questions WHERE id = :questionId")
    abstract suspend fun getQuestion(questionId: String): List<QuestionWithContent>

    @Transaction
    open suspend fun addQuestion(
        questionId: String,
        themeId: String,
        questionTuple: QuestionTuple,
        questionContent: List<QuestionContentTuple>,
        isChecked: Boolean = false
    ) {
        with(questionTuple) {
            insertQuestion(questionId, themeId, price, rightAnswer, type, isChecked)
        }
        questionContent.forEach {
            with(it) {
                addQuestionContent(questionId, type, value)
            }
        }
    }

    @Query("INSERT OR REPLACE INTO questions_content (questionId, type, value) VALUES (:questionId, :type, :value)")
    abstract suspend fun addQuestionContent(questionId: String, type: String, value: String)

    @Query("INSERT OR REPLACE INTO questions (id, themeId, price, rightAnswer, type, isChecked) VALUES (:questionId, :themeId, :price, :rightAnswer, :type, :isChecked)")
    abstract suspend fun insertQuestion(questionId: String, themeId: String, price: Int, rightAnswer: String, type: String, isChecked: Boolean)

    @Query("UPDATE questions SET isChecked = 1 WHERE id = :questionId")
    abstract suspend fun updateQuestionCheckedState(questionId: String)
}