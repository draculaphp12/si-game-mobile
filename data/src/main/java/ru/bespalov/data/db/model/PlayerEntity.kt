package ru.bespalov.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.bespalov.domain.model.Player

@Entity(
    tableName = "players"
)
internal data class PlayerEntity(
    @PrimaryKey
    val uuid: String,
    val gameId: String,
    val nickname: String,
    val score: Int = 0,
    val isModerator: Boolean
)

internal fun PlayerEntity.toPlayer(): Player = Player(
    uuid, nickname, score, isModerator
)