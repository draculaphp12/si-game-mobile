package ru.bespalov.data

import android.content.Context
import android.os.Environment
import android.util.Log
import java.io.File


class StorageRepository(
    private val context: Context
) {
    fun getPackStorageDir(directoryName: String): File {
        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            directoryName
        )
        if (!file.isDirectory) {
            if (!file.mkdirs()) {
                Log.e("SSS", "Directory not created")
            }
        }
        return file
    }

    fun getFileFromDownload(path: String): File {
        return File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            path
        )
    }
}