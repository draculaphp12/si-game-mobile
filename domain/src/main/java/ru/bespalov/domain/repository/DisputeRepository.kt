package ru.bespalov.domain.repository

import ru.bespalov.domain.model.Dispute

interface DisputeRepository {
    suspend fun addDispute(dispute: Dispute)

    suspend fun checkIfDisputeExist(questionId: String, playerId: String): Boolean
}