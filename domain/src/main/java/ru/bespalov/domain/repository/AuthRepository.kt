package ru.bespalov.domain.repository

import ru.bespalov.domain.model.User

interface AuthRepository {
    fun registration(user: User, onSuccess: () -> Unit, onFailure: (String) -> Unit)

    fun signIn(user: User, onSuccess: () -> Unit, onFailure: (String) -> Unit)

    fun getCurrentUserId(): String?
}