package ru.bespalov.domain.repository

interface PackRepository {
    suspend fun cachePack(gameId: String)
}