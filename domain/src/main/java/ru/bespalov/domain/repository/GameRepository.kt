package ru.bespalov.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.bespalov.domain.model.Game
import ru.bespalov.domain.model.Pack

interface GameRepository {
    fun createGameWithPlayer(maxPlayersCount: Int, roundsCount: Int, pack: Pack): String

    suspend fun cacheGame(gameId: String)

    fun getGameFlowFromDB(gameId: String): Flow<Game?>

    suspend fun observeGame(gameId: String)

    suspend fun updateCurrentQuestion(gameId: String, questionId: String?)

    suspend fun updateCurrentRespondent(gameId: String, playerId: String?)

    fun updateLastQuestionId(gameId: String, lastQuestionId: String)

    fun updateIsDisputeOpen(gameId: String, isDisputeOpen: Boolean)

    suspend fun updateCurrentDisputantId(gameId: String, playerId: String?)

    suspend fun increaseCurrentRound(gameId: String)
}