package ru.bespalov.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.bespalov.domain.model.Player

interface PlayerRepository {
    suspend fun cachePlayersList(gameId: String)

    fun getGamePlayersFromDB(gameId: String): Flow<List<Player>>

    suspend fun observeGamePlayersList(gameId: String)

    fun connectPlayer(gameId: String)

    fun updatePlayerScoreFirebase(playerId: String, score: Int, gameId: String)
}