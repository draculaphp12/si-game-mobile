package ru.bespalov.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.bespalov.domain.model.Round

interface RoundRepository {
    fun getRoundFlow(roundNumber: Int, gameId: String): Flow<List<Round>>
}