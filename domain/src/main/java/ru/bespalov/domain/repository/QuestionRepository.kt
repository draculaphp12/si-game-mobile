package ru.bespalov.domain.repository

import ru.bespalov.domain.model.Question

interface QuestionRepository {
    suspend fun getQuestion(questionId: String): Question?

    suspend fun updateQuestion(questionId: String)
}