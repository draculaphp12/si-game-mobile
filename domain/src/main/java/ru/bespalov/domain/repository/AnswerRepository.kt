package ru.bespalov.domain.repository

import ru.bespalov.domain.model.Answer

interface AnswerRepository {
    suspend fun addAnswerToFirebase(gameId: String, answer: Answer)

    suspend fun addFalseAnswer(gameId: String, answer: Answer, onSuccess: () -> Unit = {})

    suspend fun checkPlayerFalseAnswer(questionId: String, playerId: String): Boolean

    suspend fun checkAnswers(gameId: String)

    suspend fun observeAnswers(gameId: String)

    suspend fun getAnswer(questionId: String, playerId: String? = null, isRight: Int = 1): Answer?
}