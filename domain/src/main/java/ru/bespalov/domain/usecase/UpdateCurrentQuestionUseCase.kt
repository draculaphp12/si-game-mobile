package ru.bespalov.domain.usecase

import ru.bespalov.domain.repository.GameRepository

class UpdateCurrentQuestionUseCase(
    private val gameRepository: GameRepository
) {
    suspend fun execute(gameId: String, questionId: String?) {
        gameRepository.updateCurrentQuestion(gameId, questionId)
    }
}