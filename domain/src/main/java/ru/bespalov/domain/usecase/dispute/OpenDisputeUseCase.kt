package ru.bespalov.domain.usecase.dispute

import ru.bespalov.domain.model.Question
import ru.bespalov.domain.repository.DisputeRepository
import ru.bespalov.domain.repository.GameRepository

class OpenDisputeUseCase(
    private val gameRepository: GameRepository,
    private val disputeRepository: DisputeRepository,
) {
    suspend fun execute(lastQuestion: Question, currentUserId: String, gameId: String) {
        val isDisputeExist = disputeRepository.checkIfDisputeExist(lastQuestion.id, currentUserId)
        if (!isDisputeExist) {
            gameRepository.updateIsDisputeOpen(gameId, true)
            gameRepository.updateCurrentDisputantId(gameId, currentUserId)
        }
    }
}