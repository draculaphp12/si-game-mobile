package ru.bespalov.domain.usecase

import kotlinx.coroutines.flow.Flow
import ru.bespalov.domain.model.Round
import ru.bespalov.domain.repository.RoundRepository

class GetRoundUseCase(
    private val roundRepository: RoundRepository
) {
    fun execute(roundNumber: Int, gameId: String): Flow<List<Round>> {
        return roundRepository.getRoundFlow(roundNumber, gameId)
    }
}