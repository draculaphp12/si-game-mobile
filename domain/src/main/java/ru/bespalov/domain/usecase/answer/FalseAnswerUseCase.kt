package ru.bespalov.domain.usecase.answer

import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.repository.AnswerRepository
import ru.bespalov.domain.repository.GameRepository
import ru.bespalov.domain.repository.PlayerRepository

class FalseAnswerUseCase(
    private val answerRepository: AnswerRepository,
    private val playerRepository: PlayerRepository,
    private val gameRepository: GameRepository
) {
    suspend fun execute(
        playerAnswer: String,
        currentPlayerId: String,
        gameId: String,
        question: Question,
        onFalseAnswerSuccess: () -> Unit = {}
    ) {
        playerRepository.updatePlayerScoreFirebase(
            currentPlayerId,
            question.price * -1,
            gameId
        )
        answerRepository.addFalseAnswer(
            gameId,
            Answer(question.id, currentPlayerId, playerAnswer, false),
            onFalseAnswerSuccess
        )
        gameRepository.updateCurrentRespondent(gameId, null)
    }
}