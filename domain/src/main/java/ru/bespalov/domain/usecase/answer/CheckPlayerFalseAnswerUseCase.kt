package ru.bespalov.domain.usecase.answer

import ru.bespalov.domain.repository.AnswerRepository

class CheckPlayerFalseAnswerUseCase(
    private val answerRepository: AnswerRepository
) {
    suspend fun execute(questionId: String, playerId: String): Boolean {
        return answerRepository.checkPlayerFalseAnswer(questionId, playerId)
    }
}