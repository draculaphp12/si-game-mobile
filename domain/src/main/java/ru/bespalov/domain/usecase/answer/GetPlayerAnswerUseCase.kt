package ru.bespalov.domain.usecase.answer

import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.repository.AnswerRepository

class GetPlayerAnswerUseCase(
    private val answerRepository: AnswerRepository
) {
    suspend fun execute(questionId: String, playerId: String? = null, isRight: Int = 1): Answer? {
        return answerRepository.getAnswer(questionId, playerId, isRight)
    }
}