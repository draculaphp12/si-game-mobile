package ru.bespalov.domain.usecase.answer

import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.repository.AnswerRepository
import ru.bespalov.domain.repository.GameRepository
import ru.bespalov.domain.repository.PlayerRepository

class RightAnswerUseCase(
    private val answerRepository: AnswerRepository,
    private val playerRepository: PlayerRepository,
    private val gameRepository: GameRepository
) {
    suspend fun execute(
        playerAnswer: String,
        currentPlayerId: String,
        gameId: String,
        question: Question,
    ) {
        playerRepository.updatePlayerScoreFirebase(
            currentPlayerId,
            question.price,
            gameId
        )
        answerRepository.addAnswerToFirebase(
            gameId,
            Answer(question.id, currentPlayerId, playerAnswer, true)
        )

        gameRepository.updateCurrentRespondent(gameId, null)
        gameRepository.updateCurrentQuestion(gameId, null)
    }
}