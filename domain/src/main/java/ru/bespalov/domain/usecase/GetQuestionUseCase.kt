package ru.bespalov.domain.usecase

import ru.bespalov.domain.model.Question
import ru.bespalov.domain.repository.QuestionRepository

class GetQuestionUseCase(
    private val questionRepository: QuestionRepository
) {
    suspend fun execute(questionId: String): Question? {
        return questionRepository.getQuestion(questionId)
    }
}