package ru.bespalov.domain.usecase.dispute

import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.model.Dispute
import ru.bespalov.domain.model.Game
import ru.bespalov.domain.model.Question
import ru.bespalov.domain.repository.DisputeRepository
import ru.bespalov.domain.repository.GameRepository
import ru.bespalov.domain.repository.PlayerRepository

class AcceptDisputeUseCase(
    private val disputeRepository: DisputeRepository,
    private val playerRepository: PlayerRepository,
    private val gameRepository: GameRepository
) {
    suspend fun execute(
        game: Game,
        lastQuestion: Question?,
        currentUserId: String,
        anotherPlayerRightAnswer: Answer?
    ) {
        if (lastQuestion != null && game.currentDisputantId != null) {
            playerRepository.updatePlayerScoreFirebase(
                game.currentDisputantId!!,
                lastQuestion.price * 2,
                game.id
            )
            if (anotherPlayerRightAnswer != null) {
                playerRepository.updatePlayerScoreFirebase(
                    anotherPlayerRightAnswer.playerId ?: "-",
                    lastQuestion.price * -1,
                    game.id
                )
            }
            disputeRepository.addDispute(
                Dispute(
                    lastQuestion.id,
                    currentUserId,
                    true
                )
            )
        }
        gameRepository.updateIsDisputeOpen(game.id, false)
        gameRepository.updateCurrentDisputantId(game.id, null)
    }
}