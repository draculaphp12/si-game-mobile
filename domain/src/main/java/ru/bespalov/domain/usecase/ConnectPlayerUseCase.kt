package ru.bespalov.domain.usecase

import android.util.Log
import ru.bespalov.domain.repository.PlayerRepository

class ConnectPlayerUseCase(
    private val playerRepository: PlayerRepository
) {
    fun execute(gameId: String) {
        try {
            playerRepository.connectPlayer(gameId)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("TAG", e.message.toString())
        }
    }
}