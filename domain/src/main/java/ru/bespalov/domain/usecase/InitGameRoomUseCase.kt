package ru.bespalov.domain.usecase

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import ru.bespalov.domain.model.Player
import ru.bespalov.domain.repository.GameRepository
import ru.bespalov.domain.repository.PackRepository
import ru.bespalov.domain.repository.PlayerRepository

class InitGameRoomUseCase(
    private val packRepository: PackRepository,
    private val gameRepository: GameRepository,
    private val playerRepository: PlayerRepository
) {
    suspend fun execute(gameId: String, scope: CoroutineScope): Flow<List<Player>> {
        gameRepository.cacheGame(gameId)
        packRepository.cachePack(gameId)
        playerRepository.cachePlayersList(gameId)
        scope.launch {
            playerRepository.observeGamePlayersList(gameId)
        }
        return playerRepository.getGamePlayersFromDB(gameId)
    }
}