package ru.bespalov.domain.usecase

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import ru.bespalov.domain.model.Player
import ru.bespalov.domain.repository.PlayerRepository

class GetPlayersUseCase(
    private val playerRepository: PlayerRepository
) {
    suspend fun execute(gameId: String): Flow<List<Player>> {
        CoroutineScope(Dispatchers.IO).launch {
            playerRepository.observeGamePlayersList(gameId)
        }
        return playerRepository.getGamePlayersFromDB(gameId)
    }
}