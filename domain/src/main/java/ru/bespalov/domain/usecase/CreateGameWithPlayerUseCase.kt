package ru.bespalov.domain.usecase

import android.util.Log
import ru.bespalov.domain.model.Pack
import ru.bespalov.domain.repository.GameRepository

class CreateGameWithPlayerUseCase(
    private val gameRepository: GameRepository
) {
    fun execute(maxPlayersCount: Int, roundsCount: Int, pack: Pack): String {
        try {
            return gameRepository.createGameWithPlayer(maxPlayersCount, roundsCount, pack)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("TAG", e.message.toString())
        }
        return ""
    }
}