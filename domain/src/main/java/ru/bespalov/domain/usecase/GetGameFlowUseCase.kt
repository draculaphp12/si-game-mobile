package ru.bespalov.domain.usecase

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import ru.bespalov.domain.model.Game
import ru.bespalov.domain.repository.GameRepository

class GetGameFlowUseCase(
    private val gameRepository: GameRepository
) {
    suspend fun execute(gameId: String): Flow<Game?> {
        CoroutineScope(Dispatchers.IO).launch {
            gameRepository.observeGame(gameId)
        }
        return gameRepository.getGameFlowFromDB(gameId)
    }
}