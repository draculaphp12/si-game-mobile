package ru.bespalov.domain.usecase

import ru.bespalov.domain.repository.GameRepository

class IncreaseCurrentRoundUseCase(
    private val gameRepository: GameRepository
) {
    suspend fun execute(gameId: String, currentRound: Int, roundsCount: Int) {
        if (currentRound < roundsCount) {
            gameRepository.increaseCurrentRound(gameId)
        }
    }
}