package ru.bespalov.domain.usecase.dispute

import ru.bespalov.domain.model.Dispute
import ru.bespalov.domain.repository.DisputeRepository
import ru.bespalov.domain.repository.GameRepository

class CloseDisputeUseCase(
    private val gameRepository: GameRepository,
    private val disputeRepository: DisputeRepository,
) {
    suspend fun execute(gameId: String, lastQuestionId: String, currentUserId: String) {
        gameRepository.updateIsDisputeOpen(gameId, false)
        gameRepository.updateCurrentDisputantId(gameId, null)
        disputeRepository.addDispute(
            Dispute(
                lastQuestionId,
                currentUserId,
                false
            )
        )
    }
}