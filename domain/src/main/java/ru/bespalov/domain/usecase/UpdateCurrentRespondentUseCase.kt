package ru.bespalov.domain.usecase

import ru.bespalov.domain.repository.GameRepository

class UpdateCurrentRespondentUseCase(
    private val gameRepository: GameRepository
) {
    suspend fun execute(gameId: String, playerId: String?) {
        gameRepository.updateCurrentRespondent(gameId, playerId)
    }
}