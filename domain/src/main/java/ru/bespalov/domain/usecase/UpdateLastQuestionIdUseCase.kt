package ru.bespalov.domain.usecase

import ru.bespalov.domain.repository.GameRepository

class UpdateLastQuestionIdUseCase(
    private val gameRepository: GameRepository
) {
    fun execute(gameId: String, lastQuestionId: String) {
        gameRepository.updateLastQuestionId(gameId, lastQuestionId)
    }
}