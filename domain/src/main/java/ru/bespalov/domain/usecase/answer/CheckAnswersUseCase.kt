package ru.bespalov.domain.usecase.answer

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.bespalov.domain.repository.AnswerRepository

class CheckAnswersUseCase(
    private val answerRepository: AnswerRepository
) {
    suspend fun execute(gameId: String) {
        CoroutineScope(Dispatchers.IO).launch {
            answerRepository.observeAnswers(gameId)
        }
        answerRepository.checkAnswers(gameId)
    }
}