package ru.bespalov.domain.usecase.answer

import ru.bespalov.domain.model.Answer
import ru.bespalov.domain.repository.AnswerRepository
import ru.bespalov.domain.repository.GameRepository

class AnswerTimeOutUseCase(
    private val answerRepository: AnswerRepository,
    private val gameRepository: GameRepository
) {
    suspend fun execute(gameId: String, questionId: String?) {
        gameRepository.updateCurrentQuestion(gameId, null)
        if (questionId !== null) {
            answerRepository.addAnswerToFirebase(
                gameId,
                Answer(questionId, null, "", true)
            )
        }
    }
}