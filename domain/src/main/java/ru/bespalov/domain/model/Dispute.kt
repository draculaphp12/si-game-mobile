package ru.bespalov.domain.model

data class Dispute(
    val questionId: String,
    val playerId: String,
    val result: Boolean
)
