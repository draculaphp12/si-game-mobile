package ru.bespalov.domain.model

data class Question(
    val id: String,
    val price: Int,
    val rightAnswer: String,
    val content: List<QuestionContent>,
    val isChecked: Boolean
)

sealed class QuestionContent(var content: String) {
    class AudioQuestionContent(content: String): QuestionContent(content)
    class TextQuestionContent(content: String): QuestionContent(content)
    class VideoQuestionContent(content: String): QuestionContent(content)
    class ImageQuestionContent(content: String): QuestionContent(content)
    class SayQuestionContent(content: String): QuestionContent(content)
}