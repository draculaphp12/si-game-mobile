package ru.bespalov.domain.model

data class Player(
    val uuid: String,
    val nickname: String,
    val score: Int,
    val isModerator: Boolean
)
