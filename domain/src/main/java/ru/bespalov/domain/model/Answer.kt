package ru.bespalov.domain.model

data class Answer(
    val questionId: String,
    val playerId: String?,
    val playerAnswer: String,
    val isRight: Boolean
)
