package ru.bespalov.domain.model

data class Round(
    val id: String,
    val name: String,
    val themes: List<Theme>,
    val number: Int
)
