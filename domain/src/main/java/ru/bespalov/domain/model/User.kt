package ru.bespalov.domain.model

data class User(
    val email: String,
    val password: String,
    val nickname: String? = null
)
