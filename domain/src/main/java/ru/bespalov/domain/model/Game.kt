package ru.bespalov.domain.model

data class Game(
    val id: String = "",
    val currentPlayerId: String = "",
    val roundsCount: Int = 1,
    val currentRound: Int = 1,
    val maxPlayersCount: Int = 5,
    var currentQuestionId: String? = null,
    var currentRespondentId: String? = null,
    var lastQuestionId: String? = null,
    var isDisputeOpen: Boolean? = false,
    var currentDisputantId: String? = null
)
