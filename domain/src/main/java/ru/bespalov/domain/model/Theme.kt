package ru.bespalov.domain.model

import ru.bespalov.domain.model.Question

data class Theme(
    val id: String,
    val name: String,
    val questions: List<Question>
)
