package ru.bespalov.domain.model

data class Pack(
    val gameId: String,
    val name: String,
    val rounds: List<Round>
)
